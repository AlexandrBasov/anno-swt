/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

@Ignore
public class CsvFileTest {
	@Test
	public void test1() {
		File file = new File("/Users/matijaobreza/Downloads", "seldata-1.csv");
		Reader reader = null;
		try {
			reader = new FileReader(file);
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		}

		try {
			CsvPreference preferences = CsvPreference.STANDARD_PREFERENCE;
			CsvListReader listReader = new CsvListReader(reader, preferences);

			listReader.getHeader(true); // skip the header (can't be used with
										// CsvListReader)
			for (int i = 0; i < 100; i++) {
				List<String> row = listReader.read();
				System.err.println(ArrayUtils.toString(row));
			}

			listReader.close();

		} catch (IOException e) {
			fail(e.getMessage());
		} finally {
			IOUtils.closeQuietly(reader);
		}
	}
}
