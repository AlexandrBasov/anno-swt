package org.genesys2.anno.model;

import org.genesys2.anno.gui.AbstractModelObject;

public class DatabaseSettings extends AbstractModelObject {

    private String driverClassName="";
	private String type = "";
	private String url = "";
	private String dataSourceName = "";
	private String user = "";
	private String password = "";

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
		firePropertyChange("url", null, this.url);
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
        firePropertyChange("driverClassName", null, this.driverClassName);
    }
}
