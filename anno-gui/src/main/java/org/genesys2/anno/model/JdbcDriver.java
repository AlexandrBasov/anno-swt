package org.genesys2.anno.model;


import org.genesys2.anno.gui.AbstractModelObject;

public class JdbcDriver extends AbstractModelObject {

    private String title;
    private String driverClassName;
    private String downloadUrl;
    private String predefinedConnectUrl;

    public JdbcDriver() {

    }

    public JdbcDriver(String title) {
        this.title = title;
    }

    public JdbcDriver(String title, String driverClassName, String downloadUrl,String predefinedConnectUrl) {
        this.title = title;
        this.driverClassName = driverClassName;
        this.downloadUrl = downloadUrl;
        this.predefinedConnectUrl = predefinedConnectUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getPredefinedConnectUrl() {
        return predefinedConnectUrl;
    }

    public void setPredefinedConnectUrl(String predefinedConnectUrl) {
        this.predefinedConnectUrl = predefinedConnectUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JdbcDriver driver = (JdbcDriver) o;

        if (title != null ? !title.equals(driver.title) : driver.title != null) return false;

        return true;
    }
}
