/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.validator;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.genesys2.anno.model.Column;
import org.genesys2.anno.model.ColumnValidator;

/**
 * Validator caches regular expression patterns and evaluates input values
 * against regular expressions. Null input values and blank patterns always
 * validate.
 * 
 * @author matijaobreza
 */
public class RegexpValidator implements ColumnValidator {

	private HashMap<String, Pattern> patternCache = new HashMap<String, Pattern>(100);

	@Override
	public boolean isValid(Column column, String value) {
		if (StringUtils.isBlank(value)) {
			return true;
		}
		if (column == null || StringUtils.isBlank(column.getPattern())) {
			return true;
		}

		Pattern p = ensureMatcher(column.getPattern());
		Matcher m = p.matcher(value);

		return m.matches();
	}

	private synchronized Pattern ensureMatcher(String expression) {
		Pattern pattern = patternCache.get(expression);

		if (pattern == null) {
			patternCache.put(expression, pattern = Pattern.compile(expression));
		}

		return pattern;
	}

}
