/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.gui;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.genesys2.anno.converter.RowConverter;
import org.genesys2.anno.model.Column;
import org.genesys2.anno.model.ColumnDef;
import org.genesys2.anno.model.DatabaseSettings;
import org.genesys2.anno.reader.JDBCRowReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import swing2swt.layout.BorderLayout;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.wb.swt.SWTResourceManager;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SheetDisplay extends Composite {
	private static final Logger _log = Logger.getLogger(SheetDisplay.class);
	private DataBindingContext m_bindingContext;

	@Autowired
	private JDBCRowReader jdbcRowReader;

	@Autowired
	protected DataSourceLoader dataSourceLoader;

	@Autowired
	private ColumnDefs columnDefs;

	private static final ObjectMapper mapper = new ObjectMapper();

	private static class ContentProvider implements IStructuredContentProvider {
		@Override
		public Object[] getElements(Object inputElement) {
			if (inputElement instanceof List) {
				return ((List<?>) inputElement).toArray();
			}
			return new Object[0];
		}

		@Override
		public void dispose() {
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}

	private class TableLabelProvider extends LabelProvider implements ITableLabelProvider {
		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			if (element instanceof Object[]) {
				Object[] elArr = (Object[]) element;
				if (columnIndex < elArr.length) {
					Object el = ((Object[]) element)[columnIndex];
					return el == null ? "" : el.toString();
				} else
					return null;
			}
			return element.toString();
		}
	}

	private Table table;
	private Text txtColumnName;
	private Text txtRdfTerm;
	private DSW dsw = new DSW();
	private TableViewer tableViewer;
	private Text txtHeaderRowIndex;
	private Combo txtCsvQuoteChar;
	private Combo comboCsvSeparator;
	private Button chkbxContainsHeaders;
	private TabFolder tabFolder;
	private Text txtColumnDescription;
	private Text txtSqlQuery;
	private Button btnContainsMultipleValues;
	private IObservableValue sampleDataDswObserveValue;
	private Composite compoCSV;
	private Combo comboCharset;
	private Button btnReload;
	private Text txtSeparator;
	private Text txtRegExp;
	private Text txtGroupPattern;
	private Text txtQueryName;
	private IDataSource dataSource;
	private Button btnIncludeNullValues;

	/**
	 * Create the composite.
	 * 
	 * @param dataSourceSheet
	 * 
	 * @param parent
	 * @param style
	 */
	public SheetDisplay(IDataSource dataSource, final IDataSourceSheet dataSourceSheet, final Composite parent, final TreeViewer treeViewer, int style) {
		super(parent, SWT.NONE);
		this.dsw.setDataSourceSheet(dataSourceSheet);
		this.dataSource = dataSource;

		addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				// UNBIND ALL STUFF
				sampleDataDswObserveValue.dispose();

				@SuppressWarnings("unchecked")
				ArrayList<Object> bindings = new ArrayList<Object>(m_bindingContext.getBindings());

				for (Object b : bindings) {
					_log.trace("Remove binding " + b);
					if (b instanceof Binding) {
						((Binding) b).getModel().dispose();
					}
					m_bindingContext.removeBinding((Binding) b);
				}
				m_bindingContext.dispose();
			}
		});
		setLayout(new BorderLayout(0, 0));

		SashForm sashForm = new SashForm(this, SWT.VERTICAL);
		sashForm.setLayoutData(BorderLayout.CENTER);

		tableViewer = new TableViewer(sashForm, SWT.BORDER | SWT.FULL_SELECTION);
		tableViewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent arg0) {
				IStructuredSelection selection = (IStructuredSelection) arg0.getSelection();
				Object row = selection.getFirstElement();
				if (row == null)
					return;
				doDoubleClickOnRow((Object[]) row);
			}
		});
		table = tableViewer.getTable();
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		tabFolder = new TabFolder(sashForm, SWT.NONE);
		tabFolder.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				tabFolder.layout();
			}
		});

		TabItem tbtmQuery = new TabItem(tabFolder, SWT.NONE);
		tbtmQuery.setText("SQL Query");

		Composite compositeQuery = new Composite(tabFolder, SWT.NONE);
		compositeQuery.setLayout(new GridLayout(4, false));
		tbtmQuery.setControl(compositeQuery);

		Label lblQueryName = new Label(compositeQuery, SWT.NONE);
		lblQueryName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblQueryName.setText("Query name");

		txtQueryName = new Text(compositeQuery, SWT.BORDER);
		txtQueryName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));

		Label lblSqlQuery = new Label(compositeQuery, SWT.NONE);
		lblSqlQuery.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblSqlQuery.setText("SQL Query");

		txtSqlQuery = new Text(compositeQuery, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		GridData gdSqlQuery = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
		gdSqlQuery.minimumHeight = 50;
		txtSqlQuery.setLayoutData(gdSqlQuery);

		Button btnReloadQuery = new Button(compositeQuery, SWT.NONE);
		btnReloadQuery.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				treeViewer.refresh();
				((CTabFolder) parent).getItem(0).setText(txtQueryName.getText());
				executeSqlQuery(txtSqlQuery.getText(), dsw);

			}
		});

		btnReloadQuery.setText("Reload");
		new Label(compositeQuery, SWT.NONE);
		new Label(compositeQuery, SWT.NONE);
		new Label(compositeQuery, SWT.NONE);

		if (!dsw.getDataSourceSheet().isSqlQuery()) {
			tbtmQuery.dispose();
		}

		TabItem tbtmColumnProperties = new TabItem(tabFolder, SWT.NONE);
		tbtmColumnProperties.setText("Column");

		ScrolledComposite scrolledComposite = new ScrolledComposite(tabFolder, SWT.V_SCROLL);
		scrolledComposite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		scrolledComposite.setExpandVertical(true);
		scrolledComposite.setExpandHorizontal(true);
		tbtmColumnProperties.setControl(scrolledComposite);

		Composite composite = new Composite(scrolledComposite, SWT.V_SCROLL);
		composite.setLayout(new GridLayout(4, false));
		scrolledComposite.setContent(composite);
		composite.pack();

		Label lblThisIsA = new Label(composite, SWT.NONE);
		lblThisIsA.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblThisIsA.setText("Column name");

		txtColumnName = new Text(composite, SWT.BORDER);
		txtColumnName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));

		Label lblColumnDescription = new Label(composite, SWT.NONE);
		lblColumnDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblColumnDescription.setText("Column description");

		txtColumnDescription = new Text(composite, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtColumnDescription = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
		gd_txtColumnDescription.minimumHeight = 30;
		gd_txtColumnDescription.heightHint = 40;
		txtColumnDescription.setLayoutData(gd_txtColumnDescription);
		new Label(composite, SWT.NONE);

		btnContainsMultipleValues = new Button(composite, SWT.CHECK);
		btnContainsMultipleValues.setText("Contains multiple values, separated");

		Label lblSeparator_1 = new Label(composite, SWT.NONE);
		lblSeparator_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblSeparator_1.setText("Separator");

		txtSeparator = new Text(composite, SWT.BORDER);
		txtSeparator.setText(";");
		GridData gd_txtSeparator = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_txtSeparator.widthHint = 30;
		gd_txtSeparator.minimumWidth = 30;
		txtSeparator.setLayoutData(gd_txtSeparator);

		Label lblNewLabel = new Label(composite, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText("RDF Term");

		txtRdfTerm = new Text(composite, SWT.BORDER);
		txtRdfTerm.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));

		Label lblRegexpPattern = new Label(composite, SWT.RIGHT);
		lblRegexpPattern.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRegexpPattern.setText("RegExp pattern");

		txtRegExp = new Text(composite, SWT.BORDER);
		txtRegExp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// // Label lblData = new Label(composite, SWT.NONE);
		// // lblData.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
		// false, 1, 1));
		// // lblData.setText("Data type");
		// //
		// // Combo comboDataType = new Combo(composite, SWT.NONE);
		// // comboDataType.setItems(new String[] { "Text (general blah-blah)",
		// "Number", "Date, time, timestamp" });
		// // comboDataType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER,
		// true, false, 1, 1));
		// // new Label(composite, SWT.NONE);
		// // new Label(composite, SWT.NONE);
		// // new Label(composite, SWT.NONE);
		//
		// Button btnAnalyzeColumnData = new Button(composite, SWT.NONE);
		// btnAnalyzeColumnData.addSelectionListener(new SelectionAdapter() {
		// @Override
		// public void widgetSelected(SelectionEvent e) {
		// _log.debug("Do column analysis");
		// }
		// });
		// btnAnalyzeColumnData.setText("Analyze column data");
		// new Label(composite, SWT.NONE);
		// new Label(composite, SWT.NONE);

		DropTarget dropTarget = new DropTarget(composite, DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT | DND.DROP_LINK);
		dropTarget.setTransfer(new Transfer[] { LocalSelectionTransfer.getTransfer() });

		Label lblGroupPattern = new Label(composite, SWT.NONE);
		lblGroupPattern.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblGroupPattern.setText("Group pattern:");

		txtGroupPattern = new Text(composite, SWT.BORDER);
		txtGroupPattern.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(composite, SWT.NONE);
		
		btnIncludeNullValues = new Button(composite, SWT.CHECK);
		btnIncludeNullValues.setText("Include null values");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		dropTarget.addDropListener(new DropTargetAdapter() {
			@Override
			public void drop(DropTargetEvent event) {
				if (LocalSelectionTransfer.getTransfer().isSupportedType(event.currentDataType)) {
					IStructuredSelection sel = (IStructuredSelection) LocalSelectionTransfer.getTransfer().getSelection();
					_log.debug("Dropped " + sel);
					_log.debug("Dropped class " + sel.getClass());
					Object dragged = sel.getFirstElement();
					Column selColumn = dsw.getSelectedColumn();
					if (dragged instanceof ColumnDef && selColumn != null) {
						ColumnDef columnDef = ((ColumnDef) dragged);
						_log.info("Setting RDF term to " + columnDef.getRdfTerm());
						selColumn.setRdfTerm(columnDef.getRdfTerm());
					}
				}
			}

			@Override
			public void dragEnter(DropTargetEvent event) {
				_log.debug("dragenter " + event.detail);
				event.detail = DND.DROP_LINK;
			}
		});

		TabItem tbtmSheet = new TabItem(tabFolder, SWT.NONE);
		tbtmSheet.setText("Headers");

		Composite compoHeaders = new Composite(tabFolder, SWT.NONE);
		compoHeaders.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		tbtmSheet.setControl(compoHeaders);
		compoHeaders.setLayout(new GridLayout(2, false));
		new Label(compoHeaders, SWT.NONE);

		chkbxContainsHeaders = new Button(compoHeaders, SWT.CHECK);
		chkbxContainsHeaders.setText("Contains headers");

		Label lblRowContainingHeaders = new Label(compoHeaders, SWT.NONE);
		lblRowContainingHeaders.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRowContainingHeaders.setText("Header row index");

		txtHeaderRowIndex = new Text(compoHeaders, SWT.BORDER);
		txtHeaderRowIndex.setEnabled(false);
		txtHeaderRowIndex.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(compoHeaders, SWT.NONE);

		Button btnReload2 = new Button(compoHeaders, SWT.NONE);
		btnReload2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doDataReload();
			}
		});
		btnReload2.setText("Reload");

		txtHeaderRowIndex.addListener(SWT.Verify, new Listener() {
			@Override
			public void handleEvent(Event e) {
				String string = e.text;
				char[] chars = new char[string.length()];
				string.getChars(0, chars.length, chars, 0);
				for (int i = 0; i < chars.length; i++) {
					if (!('0' <= chars[i] && chars[i] <= '9')) {
						e.doit = false;
						return;
					}
				}
			}
		});
		if (dsw.getDataSourceSheet().isSqlQuery()) {
			tbtmSheet.dispose();
		}

		// CSV
		// if (dsw.getDataSourceSheet().isCsv()) {

		TabItem tbtmCSV = new TabItem(tabFolder, SWT.NONE);
		tbtmCSV.setText("CSV");

		ScrolledComposite scrolledComposite2 = new ScrolledComposite(tabFolder, SWT.V_SCROLL);
		scrolledComposite2.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		scrolledComposite2.setExpandVertical(true);
		scrolledComposite2.setExpandHorizontal(true);
		tbtmCSV.setControl(scrolledComposite2);

		compoCSV = new Composite(scrolledComposite2, SWT.NO_BACKGROUND);
		scrolledComposite2.setContent(compoCSV);
		compoCSV.setLayout(new GridLayout(2, false));

		Label lblCharacterSet = new Label(compoCSV, SWT.NONE);
		lblCharacterSet.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCharacterSet.setText("Character set");

		comboCharset = new Combo(compoCSV, SWT.NONE);
		{
			SortedMap<String, Charset> availableCs = Charset.availableCharsets();
			// for (String key : availableCs.keySet()) {
			// System.err.println(key);
			// }
			comboCharset.setItems(availableCs.keySet().toArray(ArrayUtils.EMPTY_STRING_ARRAY));
		}
		comboCharset.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		comboCharset.select(0);

		Label lblSeparator = new Label(compoCSV, SWT.NONE);
		lblSeparator.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblSeparator.setText("Separator");

		comboCsvSeparator = new Combo(compoCSV, SWT.READ_ONLY);

		comboCsvSeparator.setItems(new String[] { ",", "\t" });
		comboCsvSeparator.select(0);
		comboCsvSeparator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblQuoteCharacter = new Label(compoCSV, SWT.NONE);
		lblQuoteCharacter.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblQuoteCharacter.setText("Quote character");

		txtCsvQuoteChar = new Combo(compoCSV, SWT.READ_ONLY);
		txtCsvQuoteChar.setItems(new String[] { "\"", "'" });
		txtCsvQuoteChar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtCsvQuoteChar.select(0);
		new Label(compoCSV, SWT.NONE);

		btnReload = new Button(compoCSV, SWT.NONE);
		btnReload.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doDataReload();
			}
		});
		btnReload.setText("Reload");
		sashForm.setWeights(new int[] { 274, 254 });

		tableViewer.setContentProvider(new ContentProvider());
		tableViewer.setLabelProvider(new TableLabelProvider());
		m_bindingContext = initDataBindings();

		// CSV
		if (dsw.getDataSourceSheet().isCsv() && !dsw.getDataSourceSheet().isSqlQuery()) {
			initDataBindings2(m_bindingContext);
		} else {
			// remove csv tab
			tbtmCSV.dispose();
		}
		// SQL Query
		if (dsw.getDataSourceSheet().isSqlQuery()) {
			initDataBindingForSqlQuery(m_bindingContext);
			m_bindingContext.updateModels();
		}

		// Listen to rows change
		sampleDataDswObserveValue = BeanProperties.value("dataSourceSheet.sampleData").observe(dsw);
		sampleDataDswObserveValue.addChangeListener(new IChangeListener() {
			@Override
			public void handleChange(ChangeEvent arg0) {
				getDisplay().asyncExec(new Runnable() {
					@Override
					public void run() {
						updateTable();
					}
				});
			}
		});
	}

	private void executeSqlQuery(String query, DSW dsw) {
		IDataSourceSheet currentSheet = dsw.getDataSourceSheet();
		DatabaseSettings databaseSettings = ((JdbcDataSource) dataSource).getSettings();

		final List<Object[]> rows;
		try {
			rows = jdbcRowReader.getRows(databaseSettings, query, 300);
			if (rows != null) {
				currentSheet.updateData(rows);
			}
		} catch (Throwable e) {
			showMessageBox("SQL error", e);
			_log.error(e.getMessage());
		}

	}

	protected void doDoubleClickOnRow(Object[] row) {
		IDataSourceSheet dataSourceSheet = getDataSourceSheet();
		if (dataSourceSheet == null)
			return;
		Map<String, ?> map1 = RowConverter.toMap(dataSourceSheet, row, columnDefs);
		try {
			final ObjectNode jsonObject = RowConverter.toJson(map1);
			final String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
			SwtUtil.showMessageBox(getShell(), "Genesys JSON Preview", prettyJson);
		} catch (Exception e) {
			_log.error(e.getMessage(), e);
			showMessageBox("Conversion error", e);
		}
	}

	private void showMessageBox(String title, Throwable e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		pw.println(e.getMessage());
		e.printStackTrace(pw);
		sw.flush();
		SwtUtil.showMessageBox(getShell(), title, sw.toString());
	}

	protected void doDataReload() {
		_log.info("Force reload of CSV data sheet");
		IDataSourceSheet currentSheet = dsw.getDataSourceSheet();
		try {
			final List<Object[]> rows = dataSourceLoader.loadRows(currentSheet, 200);
			currentSheet.updateData(rows);

		} catch (Throwable t) {
			_log.error("Error reloading file: " + t.getMessage(), t);
		}
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	protected TableViewer getTableViewer() {
		return tableViewer;
	}

	private void updateTable() {
		_log.debug("Rebuilding table...");
		table.clearAll();
		for (TableColumn tc : table.getColumns()) {
			tc.dispose();
		}

		IDataSourceSheet dataSourceSheet = dsw.getDataSourceSheet();

		// Generate table columns
		for (final Column column : dataSourceSheet.getColumns()) {
			final TableColumn c = new TableColumn(table, SWT.NONE);
			c.setText(column.getPreferredName());
			c.setWidth(100);
			c.addListener(SWT.Selection, new Listener() {
				@Override
				public void handleEvent(Event event) {
					_log.debug("Selected " + column);
					dsw.setSelectedColumn(column);
				}
			});
		}

		tableViewer.setInput(dataSourceSheet.getSampleData());
		_log.info("Done with table update");
	}

	public IDataSourceSheet getDataSourceSheet() {
		return dsw.getDataSourceSheet();
	}

	public IDataSource getDataSource() {
		return dataSource;
	}

	private void initDataBindingForSqlQuery(DataBindingContext bindingContext) {
		IObservableValue obTextQueryName = WidgetProperties.text(SWT.Modify).observe(txtQueryName);
		IObservableValue queryNameValue = BeanProperties.value("dataSourceSheet.sheetName").observe(dsw);
		bindingContext.bindValue(obTextQueryName, queryNameValue, null, null);
		//
		IObservableValue obTextSqlQuery = WidgetProperties.text(SWT.Modify).observe(txtSqlQuery);
		IObservableValue sqlQueryValue = BeanProperties.value("dataSourceSheet.query").observe(dsw);
		bindingContext.bindValue(obTextSqlQuery, sqlQueryValue, null, null);
	}

	private void initDataBindings2(DataBindingContext bindingContext) {
		//
		IObservableValue observeSelectionComboCharsetObserveWidget = WidgetProperties.selection().observe(comboCharset);
		IObservableValue csvDataSourceSheetcharsetDswObserveValue = BeanProperties.value("csvDataSourceSheet.charset").observe(dsw);
		bindingContext.bindValue(observeSelectionComboCharsetObserveWidget, csvDataSourceSheetcharsetDswObserveValue, null, null);
		//
		IObservableValue observeTextTxtCsvSeparatorObserveWidget = WidgetProperties.selection().observe(comboCsvSeparator);
		IObservableValue dataSourceSheetsheetDelimiterDswObserveValue = BeanProperties.value("csvDataSourceSheet.delimiterChar").observe(dsw);
		bindingContext.bindValue(observeTextTxtCsvSeparatorObserveWidget, dataSourceSheetsheetDelimiterDswObserveValue, null, null);
		//
		IObservableValue observeTextTxtCsvQuoteCharObserveWidget = WidgetProperties.selection().observe(txtCsvQuoteChar);
		IObservableValue dataSourceSheetsheetQuoteCharDswObserveValue = BeanProperties.value("csvDataSourceSheet.quoteChar").observe(dsw);
		bindingContext.bindValue(observeTextTxtCsvQuoteCharObserveWidget, dataSourceSheetsheetQuoteCharDswObserveValue, null, null);

	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue observeSelectionChkbxContainsHeadersObserveWidget_1 = WidgetProperties.selection().observe(chkbxContainsHeaders);
		IObservableValue headersIncludedGetDataSourceSheetObserveValue = PojoProperties.value("dataSourceSheet.headersIncluded").observe(dsw);
		bindingContext.bindValue(observeSelectionChkbxContainsHeadersObserveWidget_1, headersIncludedGetDataSourceSheetObserveValue, null, null);
		//
		IObservableValue observeTextTxtHeaderRowIndexObserveWidget_1 = WidgetProperties.text(SWT.Modify).observe(txtHeaderRowIndex);
		IObservableValue headerRowIndexGetDataSourceSheetObserveValue = PojoProperties.value("dataSourceSheet.headerRowIndex").observe(dsw);
		bindingContext.bindValue(observeTextTxtHeaderRowIndexObserveWidget_1, headerRowIndexGetDataSourceSheetObserveValue, null, null);
		//
		IObservableValue observeEnabledTxtHeaderRowIndexObserveWidget = WidgetProperties.enabled().observe(txtHeaderRowIndex);
		IObservableValue observeSelectionChkbxContainsHeadersObserveWidget = WidgetProperties.selection().observe(chkbxContainsHeaders);
		bindingContext.bindValue(observeEnabledTxtHeaderRowIndexObserveWidget, observeSelectionChkbxContainsHeadersObserveWidget, null, null);
		//
		IObservableValue observeTextTxtRdfTermObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtRdfTerm);
		IObservableValue selectedColumnrdfTermDswObserveValue = BeanProperties.value("selectedColumn.rdfTerm").observe(dsw);
		bindingContext.bindValue(observeTextTxtRdfTermObserveWidget, selectedColumnrdfTermDswObserveValue, null, null);
		//
		IObservableValue observeSelectionBtnContainsMultipleValuesObserveWidget = WidgetProperties.selection().observe(btnContainsMultipleValues);
		IObservableValue selectedColumnmultipleDswObserveValue = BeanProperties.value("selectedColumn.multiple").observe(dsw);
		bindingContext.bindValue(observeSelectionBtnContainsMultipleValuesObserveWidget, selectedColumnmultipleDswObserveValue, null, null);
		//
		IObservableValue observeTextTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtColumnDescription);
		IObservableValue selectedColumndescriptionDswObserveValue = BeanProperties.value("selectedColumn.description").observe(dsw);
		bindingContext.bindValue(observeTextTextObserveWidget, selectedColumndescriptionDswObserveValue, null, null);
		//
		IObservableValue observeTextTxtColumnNameObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtColumnName);
		IObservableValue selectedColumnpreferredNameDswObserveValue = BeanProperties.value("selectedColumn.preferredName").observe(dsw);
		bindingContext.bindValue(observeTextTxtColumnNameObserveWidget, selectedColumnpreferredNameDswObserveValue, null, null);
		//
		IObservableValue observeEnabledTxtSeparatorObserveWidget = WidgetProperties.enabled().observe(txtSeparator);
		IObservableValue observeSelectionBtnContainsMultipleValuesObserveWidget_1 = WidgetProperties.selection().observe(btnContainsMultipleValues);
		bindingContext.bindValue(observeEnabledTxtSeparatorObserveWidget, observeSelectionBtnContainsMultipleValuesObserveWidget_1, null, null);
		//
		IObservableValue observeTextTxtRegExpObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtRegExp);
		IObservableValue selectedColumnpatternDswObserveValue = BeanProperties.value("selectedColumn.pattern").observe(dsw);
		bindingContext.bindValue(observeTextTxtRegExpObserveWidget, selectedColumnpatternDswObserveValue, null, null);
		//
		IObservableValue observeTextTxtSeparatorObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtSeparator);
		IObservableValue selectedColumnseparatorDswObserveValue = BeanProperties.value("selectedColumn.separator").observe(dsw);
		bindingContext.bindValue(observeTextTxtSeparatorObserveWidget, selectedColumnseparatorDswObserveValue, null, null);
		//
		IObservableValue observeTextTxtGroupPatternObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtGroupPattern);
		IObservableValue selectedColumngroupPatternDswObserveValue = BeanProperties.value("selectedColumn.groupPattern").observe(dsw);
		bindingContext.bindValue(observeTextTxtGroupPatternObserveWidget, selectedColumngroupPatternDswObserveValue, null, null);
		//
		IObservableValue observeSelectionBtnIncludeNullValuesObserveWidget = WidgetProperties.selection().observe(btnIncludeNullValues);
		IObservableValue selectedColumnincludeNullDswObserveValue = BeanProperties.value("selectedColumn.includeNull").observe(dsw);
		bindingContext.bindValue(observeSelectionBtnIncludeNullValuesObserveWidget, selectedColumnincludeNullDswObserveValue, null, null);
		//
		return bindingContext;
	}
}
