package org.genesys2.anno.gui;

import java.io.File;
import java.net.MalformedURLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.genesys2.anno.model.DatabaseSettings;
import org.genesys2.anno.model.JdbcDrivers;
import org.genesys2.anno.model.OAuthSettings;
import org.genesys2.anno.model.Settings;
import org.genesys2.anno.parser.CsvDataSourceParser;
import org.genesys2.anno.parser.XlsxDataSourceParser;
import org.genesys2.anno.reader.JDBCRowReader;
import org.genesys2.anno.util.ConnectionUtils;
import org.genesys2.client.oauth.GenesysClient;
import org.genesys2.client.oauth.api.GenesysApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * Main Spring configuration
 * 
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = { "org.genesys2.anno.gui" })
public class SpringConfig {
	private static final Logger _log = Logger.getLogger(SpringConfig.class);

	@Value("${genesys.magic.workspace}")
	private String workspacePath;

	@Bean
	public DataSourceLoader dataSourceLoader() {
		DataSourceLoaderImpl dataSourceLoader = new DataSourceLoaderImpl();
		dataSourceLoader.registerParser(new XlsxDataSourceParser());
		dataSourceLoader.registerParser(new CsvDataSourceParser());
		return dataSourceLoader;
	}

    @Bean
    public JdbcDrivers jdbcDrivers() {
        return new JdbcDrivers();
    }

	@Bean
	public ExecutorService getExecutorService() {
		return Executors.newFixedThreadPool(5);
	}

	@Bean
	public ColumnDefs getColumnDefs() {
		System.err.println("Making columnDefs");
		return new ColumnDefs();
	}

	@Bean
	public Settings getSettings() {
		System.err.println("Creating new settings");
		return new Settings();
	}

	@Bean
	public DatabaseSettings getDatabaseSettings() {
		return new DatabaseSettings();
	}

	@Bean
	public DwcaBuilder dwcaBuilder() {
		return new DwcaBuilder();
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public JDBCRowReader jdbcRowReader() {
		return new JDBCRowReader();
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public SheetDisplay sheetDisplay(IDataSource dataSource,IDataSourceSheet dataSourceSheet, Composite parent, TreeViewer treeViewer, int style) {
		return new SheetDisplay(dataSource,dataSourceSheet, parent, treeViewer, style);
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public PushDialog pushDialog(Shell shell, int style) {
		return new PushDialog(shell, style);
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public SettingsDialog settingsDialog(Shell shell, int style) {
		return new SettingsDialog(shell, style);
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public DatabaseDialog databaseDialog(Shell shell, TreeViewer treeViewer, int style) {
		return new DatabaseDialog(shell, treeViewer, style);
	}

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	public ConnectionUtils connectionUtils() {
		System.err.println("Loading connectionUtils");
		return new ConnectionUtils();
	}

	/**
	 * Extra classLoader for JDBC drivers
	 * 
	 * @return
	 */
	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	public ExtraClassLoader jdbcDriverClassLoader() {
		_log.info("Updating classloader.");

		ClassLoader currentThreadClassLoader = Thread.currentThread().getContextClassLoader();
		ExtraClassLoader extraClassLoader = new ExtraClassLoader(currentThreadClassLoader);

		File baseDir = new File(workspacePath, "jdbc");
		if (baseDir.exists()) {
			try {
				extraClassLoader.addJars(baseDir);
			} catch (MalformedURLException e) {
				_log.warn(e, e);
			}
		} else {
			try {
				baseDir.mkdir();
				_log.info("Extensions directory created at " + baseDir.getAbsolutePath());
			} catch (Throwable e) {
				_log.error("Could not create extensions directory: " + e, e);
			}
		}

		return extraClassLoader;
	}

	@Bean
	public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
		final PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
		// Need to ignore "genesys.properties" if not found
		propertyPlaceholderConfigurer.setIgnoreResourceNotFound(true);

		propertyPlaceholderConfigurer.setLocations(new Resource[] { new ClassPathResource("genesys.properties"), new ClassPathResource("local.properties") });

		return propertyPlaceholderConfigurer;
	}

	// Not a bean
	public static GenesysClient createGenesysClient(OAuthSettings oauthSettings) {
		GenesysClient genesysClient = new GenesysClient();
		genesysClient.setGenesysApi(new GenesysApi());
		genesysClient.setBaseUrl(oauthSettings.getServerUrl());
		genesysClient.setAccessToken(oauthSettings.getAccessToken());
		genesysClient.setRefreshToken(oauthSettings.getRefreshToken());
		genesysClient.connect(oauthSettings.getClientKey(), oauthSettings.getClientSecret(), "oob");
		return genesysClient;
	}
}
