package org.genesys2.anno.gui;

import java.io.File;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import swing2swt.layout.BorderLayout;

public class WorkspaceDialog extends Dialog {

	protected Object result;
	protected Shell shlWorkspaceLauncher;
	private Text txtWorkspacePath;
	private Label lblWorkspaceError;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Display display = Display.getDefault();
			Shell sh = new Shell(display);
			WorkspaceDialog wd = new WorkspaceDialog(sh, SWT.NONE);
			Object res = wd.open();
			System.err.println("Selected: " + res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public WorkspaceDialog(Shell parent, int style) {
		super(parent, style);
		setText("Workspace Launcher");
	}

	public WorkspaceDialog(Shell parent) {
		super(parent, SWT.NONE);
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shlWorkspaceLauncher.open();
		shlWorkspaceLauncher.layout();
		Display display = getParent().getDisplay();
		while (!shlWorkspaceLauncher.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlWorkspaceLauncher = new Shell(getParent(), SWT.CLOSE | SWT.TITLE);
		shlWorkspaceLauncher.setSize(600, 168);
		shlWorkspaceLauncher.setText("Workspace Launcher");
		shlWorkspaceLauncher.setLayout(new BorderLayout(0, 0));

		Composite composite = new Composite(shlWorkspaceLauncher, SWT.NONE);
		composite.setLayoutData(BorderLayout.CENTER);
		composite.setLayout(new GridLayout(3, false));

		Label lblWorkspace = new Label(composite, SWT.NONE);
		lblWorkspace.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblWorkspace.setText("Workspace:");

		txtWorkspacePath = new Text(composite, SWT.BORDER);
		txtWorkspacePath.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				doCheckPath();
			}
		});
		txtWorkspacePath.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Button btnBrowse = new Button(composite, SWT.NONE);
		btnBrowse.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doBrowseWorkspace();
			}
		});
		btnBrowse.setText("Browse...");
		new Label(composite, SWT.NONE);

		lblWorkspaceError = new Label(composite, SWT.NONE);
		lblWorkspaceError.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		lblWorkspaceError.setText("Cannot use selected directory as workspace!");
		lblWorkspaceError.setVisible(false);
		new Label(composite, SWT.NONE);

		Composite composite_1 = new Composite(shlWorkspaceLauncher, SWT.NONE);
		composite_1.setLayoutData(BorderLayout.SOUTH);
		RowLayout rl_composite_1 = new RowLayout(SWT.HORIZONTAL);
		rl_composite_1.fill = true;
		rl_composite_1.wrap = false;
		rl_composite_1.pack = false;
		composite_1.setLayout(rl_composite_1);

		Button btnCancel = new Button(composite_1, SWT.NONE);
		btnCancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				result = null;
				shlWorkspaceLauncher.dispose();
			}
		});
		btnCancel.setText("Cancel");

		Button btnOk = new Button(composite_1, SWT.NONE);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doWorkspaceSelected();
			}
		});
		btnOk.setText("OK");

		Composite composite_2 = new Composite(shlWorkspaceLauncher, SWT.NONE);
		composite_2.setBackground(SWTResourceManager.getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND));
		composite_2.setLayoutData(BorderLayout.NORTH);
		FillLayout fl_composite_2 = new FillLayout(SWT.HORIZONTAL);
		fl_composite_2.marginWidth = 5;
		fl_composite_2.marginHeight = 10;
		composite_2.setLayout(fl_composite_2);

		Label lblSelectWorkspaceDirectory = new Label(composite_2, SWT.WRAP | SWT.CENTER);
		lblSelectWorkspaceDirectory.setBackground(SWTResourceManager.getColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND));
		lblSelectWorkspaceDirectory.setText("Select workspace directory where your configuration and databases are stored.");

	}

	protected void doWorkspaceSelected() {
		doCheckPath();

		File file = new File(txtWorkspacePath.getText());
		if (checkSelectedPath(file.getAbsolutePath())) {
			ensureWorkspace(file.getAbsolutePath());

			result = file.getAbsolutePath();
			shlWorkspaceLauncher.dispose();
		} else {
			result = null;
		}
	}

	private void ensureWorkspace(String path) {
		File absolutePath = new File(path);
		absolutePath.mkdirs();
		File localProperties = new File(absolutePath, "local.properties");
		if (localProperties.exists()) {

		} else {
			try {
				localProperties.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	protected void doBrowseWorkspace() {

		DirectoryDialog dialog = new DirectoryDialog(shlWorkspaceLauncher, SWT.OPEN);
		dialog.setMessage("This is where your configuration and databases will be stored.");
		dialog.setText("Select Workspace directory to use");
		dialog.setFilterPath(txtWorkspacePath.getText());
		String path = dialog.open();

		if (path != null) {
			txtWorkspacePath.setText(path);
			txtWorkspacePath.setSelection(path.length());
			doCheckPath();
		}
	}

	private void doCheckPath() {
		result = null;

		if (checkSelectedPath(txtWorkspacePath.getText())) {
			lblWorkspaceError.setVisible(false);
		} else {
			lblWorkspaceError.setVisible(true);
		}
	}

	private boolean checkSelectedPath(String directoryPath) {
		File path = new File(directoryPath);
		File localProperties = new File(path, "local.properties");

		if (path.exists()) {
			System.err.println("Path exists");
			// Check if workspace or blank
			if (localProperties.exists()) {
				System.err.println("Found local.properties");
				return true;
			} else if (path.list().length == 0) {
				System.err.println("Path has no files");
				return true;
			} else {
				System.err.println("Other");
				return false;
			}
		} else {
			System.err.println("Path doesnt exist");
			return true;
		}
	}

}
