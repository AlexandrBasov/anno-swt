package org.genesys2.anno.gui;

import org.apache.commons.lang3.StringUtils;
import org.genesys2.anno.model.Column;
import org.genesys2.anno.model.ColumnDef;
import org.genesys2.anno.model.DatabaseSettings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class JdbcDataSourceSheet extends AbstractModelObject implements IDataSourceSheet {

	private String sheetName = "Unnamed query";
	private boolean headersIncluded = true;
	private int headerRowIndex = 0;
	private String query;
	private List<Object[]> sampleData;
	private List<Column> columns = new ArrayList<Column>();

	@Override
	public String getQuery() {
		return query;
	}

	@Override
	public void setQuery(String query) {
		this.query = query;
	}

	@Override
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	@Override
	public String getSheetName() {
		return this.sheetName;
	}

	@Override
	public void setHeadersIncluded(boolean headersIncluded) {
		this.headersIncluded = headersIncluded;
		firePropertyChange("headersIncluded", null, headersIncluded);
	}

	@Override
	public boolean isHeadersIncluded() {
		return this.headersIncluded;
	}

	@Override
	public void setHeaderRowIndex(int headerRowIndex) {
		this.headerRowIndex = headerRowIndex;
		firePropertyChange("headerRowIndex", null, headerRowIndex);
	}

	@Override
	public int getHeaderRowIndex() {
		return this.headerRowIndex;
	}

	@Override
	public void updateData(List<Object[]> rows) {
		int columnCount = 0;
		for (Object[] row : rows) {
			if (row != null)
				columnCount = Math.max(columnCount, row.length);
		}

		Object[] headers = null;

		if (headersIncluded) {
			try {
				this.sampleData = rows.subList(headerRowIndex + 1, rows.size());
				Object[] headerRow = rows.get(headerRowIndex);
				headers = headerRow;
			} catch (IndexOutOfBoundsException e) {

			} catch (IllegalArgumentException e) {

			}
		} else {
			this.sampleData = rows;
		}

		// Make sure we have the Columns in the list
		ensureColumns(columnCount, headers);

		firePropertyChange("sampleData", null, this.sampleData);
	}

	private void ensureColumns(int columnCount, Object[] headerRow) {
		boolean changed = false;
		for (int i = columns.size() - 1; i >= 0 && i > columnCount - 1; i--) {
			columns.remove(i);
			changed = true;
		}
		for (int i = columns.size(); i < columnCount; i++) {
			columns.add(new Column());
			changed = true;
		}

		for (int i = columns.size() - 1; i >= 0; i--) {
			String headerColumnName = (headerRow != null && headerRow.length > i ? String.valueOf(headerRow[i]) : null);
			Column c = columns.get(i);
			c.setPreferredName(headerColumnName == null ? "Column " + i : headerColumnName);
		}

		// fire change
		if (changed)
			firePropertyChange("columns", null, columns);
	}

	@Override
	public List<Object[]> getSampleData() {
		return this.sampleData;
	}

	@Override
	public List<Column> getColumns() {
		return this.columns;
	}

	@Override
	public void setColumns(List<Column> columns) {
		this.columns = columns;
		firePropertyChange("columns", null, columns);
	}

	@Override
	public boolean isCsv() {
		return false;
	}

	@Override
	public boolean isSqlQuery() {
		return true;
	}

	@Override
	public File getSourceFile() {
		return null;
	}

	@Override
	public void automap(ColumnDefs columnDefs) {
		for (ColumnDef columnDef : columnDefs.getColumnDefs()) {
			// _log.debug("Looking at " + columnDef.getPreferredName());
			for (Column column : columns) {
				if (StringUtils.isBlank(column.getRdfTerm())) {
					// Change only if blank
					if (columnDef.getPreferredName().equalsIgnoreCase(column.getPreferredName())) {
						column.setRdfTerm(columnDef.getRdfTerm());
						column.setMultiple(columnDef.hasAllowMultiple());
						column.setDescription(columnDef.getTitle());
					}
				}
			}
		}
	}

	@Override
	public String toString() {
		return this.sheetName;
	}

}
