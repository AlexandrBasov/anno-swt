/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.genesys2.anno.parser.RowReader;

public interface DataSourceLoader {

	void registerParser(DataSourceParser parser);

	DataSourceFile loadDataSource(File file);

	List<Object[]> loadRows(IDataSourceSheet sheet, int maxRows) throws UnsupportedDataFormatException, FileNotFoundException, IOException;

	List<Object[]> loadDataRows(DataSourceSheet sheet, int maxRows) throws UnsupportedDataFormatException, FileNotFoundException, IOException;

	RowReader createRowReader(IDataSourceSheet sheet,IDataSource dataSource) throws IOException, UnsupportedDataFormatException;
}
