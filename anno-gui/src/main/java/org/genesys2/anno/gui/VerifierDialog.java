package org.genesys2.anno.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class VerifierDialog extends Dialog {

	protected Shell shlEstablishLinkTo;
	private Text txtVerifier;
	private String verifierCode;
	private String authorizationUrl;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public VerifierDialog(Shell parent, int style) {
		super(parent, SWT.DIALOG_TRIM);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * 
	 * @param authorizationUrl
	 * @return the result
	 */
	public String open(String authorizationUrl) {
		this.authorizationUrl = authorizationUrl;
		createContents();
		shlEstablishLinkTo.open();
		shlEstablishLinkTo.layout();
		Display display = getParent().getDisplay();
		while (!shlEstablishLinkTo.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return verifierCode;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlEstablishLinkTo = new Shell(getParent(), getStyle());
		shlEstablishLinkTo.setSize(491, 199);
		shlEstablishLinkTo.setText("Establish link to server");
		GridLayout gl_shlEstablishLinkTo = new GridLayout(2, false);
		gl_shlEstablishLinkTo.marginRight = 10;
		gl_shlEstablishLinkTo.marginLeft = 10;
		shlEstablishLinkTo.setLayout(gl_shlEstablishLinkTo);

		Label lblLoginWithGenesys = new Label(shlEstablishLinkTo, SWT.NONE);
		lblLoginWithGenesys.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblLoginWithGenesys.setText("Login with Genesys PGR server and authorize the request.");
		new Label(shlEstablishLinkTo, SWT.NONE);

		Composite composite_1 = new Composite(shlEstablishLinkTo, SWT.NONE);
		composite_1.setLayout(new RowLayout(SWT.HORIZONTAL));

		Button btnOpenLink = new Button(composite_1, SWT.NONE);
		btnOpenLink.setToolTipText("This will open a browser window!");
		btnOpenLink.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				org.eclipse.swt.program.Program.launch(authorizationUrl);
			}
		});
		btnOpenLink.setText("Open link in browser");

		Button btnCopyUrlTo = new Button(composite_1, SWT.NONE);
		btnCopyUrlTo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Clipboard clipboard = new Clipboard(getParent().getDisplay());
				clipboard.setContents(new Object[] { authorizationUrl }, new Transfer[] { TextTransfer.getInstance() });
			}
		});
		btnCopyUrlTo.setText("Copy URL to clipboard");

		Label lblAfterAuthorizingThe = new Label(shlEstablishLinkTo, SWT.NONE);
		lblAfterAuthorizingThe.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblAfterAuthorizingThe.setText("After authorizing the request, copy the verifier code and paste it here:");

		Label lblVerifierCode = new Label(shlEstablishLinkTo, SWT.NONE);
		lblVerifierCode.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblVerifierCode.setText("Verifier code");

		txtVerifier = new Text(shlEstablishLinkTo, SWT.BORDER);
		txtVerifier.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				verifierCode = txtVerifier.getText();
			}
		});
		txtVerifier.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		new Label(shlEstablishLinkTo, SWT.NONE);

		Composite composite = new Composite(shlEstablishLinkTo, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new RowLayout(SWT.HORIZONTAL));

		Button btnOk = new Button(composite, SWT.NONE);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shlEstablishLinkTo.dispose();
			}
		});
		btnOk.setText("OK");
	}

}
