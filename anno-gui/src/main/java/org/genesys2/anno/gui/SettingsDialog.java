package org.genesys2.anno.gui;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.genesys2.anno.model.OAuthSettings;
import org.genesys2.anno.model.Settings;
import org.genesys2.client.oauth.GenesysApiException;
import org.genesys2.client.oauth.GenesysClient;
import org.genesys2.client.oauth.OAuthAuthenticationException;
import org.genesys2.client.oauth.PleaseRetryException;
import org.scribe.exceptions.OAuthConnectionException;
import org.springframework.beans.factory.annotation.Autowired;

import swing2swt.layout.BorderLayout;

public class SettingsDialog extends Dialog {
	private static final Logger _log = Logger.getLogger(SettingsDialog.class);
	private DataBindingContext m_bindingContext;

	@Autowired
	private Settings settings;

	protected Object result;
	protected Shell shell;
	private Text txtGenesysURL;
	private Text txtAuthorizationEndpoint;
	private Text txtTokenEndpoint;
	private Text txtApiUrl;
	private Text txtAccessToken;
	private Text txtRefreshToken;
	private Text txtClientKey;
	private Text txtClientSecret;

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public SettingsDialog(Shell parent, int style) {
		super(parent, style);
		setText("Settings");
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.SHELL_TRIM);
		shell.setSize(665, 445);
		shell.setText(getText());
		shell.setLayout(new BorderLayout(0, 0));

		TabFolder tabFolder = new TabFolder(shell, SWT.NONE);
		tabFolder.setLayoutData(BorderLayout.CENTER);

		TabItem tbtmGenesysApi = new TabItem(tabFolder, SWT.NONE);
		tbtmGenesysApi.setText("Genesys API");

		ScrolledComposite scrolledComposite = new ScrolledComposite(tabFolder, SWT.V_SCROLL);
		scrolledComposite.setShowFocusedControl(true);
		tbtmGenesysApi.setControl(scrolledComposite);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		Composite composite = new Composite(scrolledComposite, SWT.NONE);
		composite.setLayout(new GridLayout(1, false));

		Group grpGenesysApiConfiguration = new Group(composite, SWT.NONE);
		grpGenesysApiConfiguration.setLayout(new GridLayout(2, false));
		grpGenesysApiConfiguration.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		grpGenesysApiConfiguration.setText("Genesys API configuration");

		Label lblGenesysServer = new Label(grpGenesysApiConfiguration, SWT.NONE);
		GridData gd_lblGenesysServer = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblGenesysServer.widthHint = 150;
		lblGenesysServer.setLayoutData(gd_lblGenesysServer);
		lblGenesysServer.setAlignment(SWT.RIGHT);
		lblGenesysServer.setText("Genesys Server URL");

		txtGenesysURL = new Text(grpGenesysApiConfiguration, SWT.BORDER);
		txtGenesysURL.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblAuthorizationEndpoint = new Label(grpGenesysApiConfiguration, SWT.NONE);
		lblAuthorizationEndpoint.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblAuthorizationEndpoint.setAlignment(SWT.RIGHT);
		lblAuthorizationEndpoint.setText("Authorization endpoint");

		txtAuthorizationEndpoint = new Text(grpGenesysApiConfiguration, SWT.BORDER);
		txtAuthorizationEndpoint.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblTokenEndpoint = new Label(grpGenesysApiConfiguration, SWT.NONE);
		lblTokenEndpoint.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblTokenEndpoint.setAlignment(SWT.RIGHT);
		lblTokenEndpoint.setText("Token endpoint");

		txtTokenEndpoint = new Text(grpGenesysApiConfiguration, SWT.BORDER);
		txtTokenEndpoint.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblGenesysApiUrl = new Label(grpGenesysApiConfiguration, SWT.NONE);
		lblGenesysApiUrl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		lblGenesysApiUrl.setAlignment(SWT.RIGHT);
		lblGenesysApiUrl.setText("Genesys API URL");

		txtApiUrl = new Text(grpGenesysApiConfiguration, SWT.BORDER);
		txtApiUrl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblClientApiKey = new Label(grpGenesysApiConfiguration, SWT.NONE);
		lblClientApiKey.setAlignment(SWT.RIGHT);
		lblClientApiKey.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblClientApiKey.setText("Client API key");

		txtClientKey = new Text(grpGenesysApiConfiguration, SWT.BORDER);
		txtClientKey.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblClientSecret = new Label(grpGenesysApiConfiguration, SWT.NONE);
		lblClientSecret.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblClientSecret.setText("Client secret");
		lblClientSecret.setAlignment(SWT.RIGHT);

		txtClientSecret = new Text(grpGenesysApiConfiguration, SWT.BORDER);
		txtClientSecret.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Group grpOauthTokens = new Group(composite, SWT.NONE);
		grpOauthTokens.setLayout(new GridLayout(2, false));
		grpOauthTokens.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		grpOauthTokens.setText("OAuth Tokens");

		Label lblAccessToken = new Label(grpOauthTokens, SWT.NONE);
		lblAccessToken.setAlignment(SWT.RIGHT);
		GridData gd_lblAccessToken = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_lblAccessToken.widthHint = 150;
		lblAccessToken.setLayoutData(gd_lblAccessToken);
		lblAccessToken.setText("Access token");

		txtAccessToken = new Text(grpOauthTokens, SWT.BORDER);
		txtAccessToken.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblRefreshToken = new Label(grpOauthTokens, SWT.NONE);
		lblRefreshToken.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRefreshToken.setText("Refresh token");

		txtRefreshToken = new Text(grpOauthTokens, SWT.BORDER);
		txtRefreshToken.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Composite composite_1 = new Composite(composite, SWT.NONE);
		GridData gd_composite_1 = new GridData(SWT.LEFT, SWT.TOP, true, false, 1, 1);
		gd_composite_1.horizontalIndent = 160;
		composite_1.setLayoutData(gd_composite_1);
		composite_1.setLayout(new RowLayout(SWT.HORIZONTAL));

		Button btnClearTokens = new Button(composite_1, SWT.NONE);
		btnClearTokens.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				settings.getOauthSettings().clearTokens();
			}
		});
		btnClearTokens.setText("Clear tokens");

		Button btnAuthenticate = new Button(composite_1, SWT.NONE);
		btnAuthenticate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doAuthenticate();
			}
		});
		btnAuthenticate.setText("Authenticate");

		composite.pack();
		scrolledComposite.setContent(composite);
		scrolledComposite.setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		TabItem tbtmTodo = new TabItem(tabFolder, SWT.NONE);
		tbtmTodo.setText("TODO");

		m_bindingContext = initDataBindings();
	}

	protected void doAuthenticate() {
		OAuthSettings oauthSettings = settings.getOauthSettings();
		GenesysClient genesysClient = null;

		try {
			genesysClient = SpringConfig.createGenesysClient(oauthSettings);
		} catch (IllegalArgumentException e) {
			txtClientKey.forceFocus();
			return;
		}

		try {
			genesysClient.me();
			oauthSettings.setAccessToken(genesysClient.getAccessToken().getToken());

			SwtUtil.showMessageBox(getParent(), "OAuth", "Tokens are up to date.");

		} catch (OAuthConnectionException e) {
			_log.error(e.getMessage(), e);
			SwtUtil.showMessageBox(getParent(), "Problem connecting to server", "Could not access remote service at:\n" + oauthSettings.getServerUrl() + "\n\n" + e.getMessage());
		} catch (OAuthAuthenticationException e) {
			_log.warn(e.getMessage());
			String authorizationUrl = genesysClient.getAuthorizationUrl(null);
			VerifierDialog vd = new VerifierDialog(getParent(), SWT.NONE);
			String verifierCode = vd.open(authorizationUrl).trim();
			_log.info("Got verifier code: " + verifierCode);
			if (StringUtils.isNotBlank(verifierCode)) {
				try {
					genesysClient.authenticate(verifierCode);
					oauthSettings.setAccessToken(genesysClient.getAccessToken().getToken());
					oauthSettings.setRefreshToken(genesysClient.getRefreshToken().getToken());
				} catch (Throwable e1) {
					_log.error(e1.getMessage(), e1);
					SwtUtil.showMessageBox(getParent(), "OAuth error", e1.getMessage());
				}
			}

		} catch (PleaseRetryException e) {
			_log.error("Please retry", e);
		} catch (GenesysApiException e) {
			_log.error("Please retry", e);
		}
	}

	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue observeTextTxtGenesysURLObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtGenesysURL);
		IObservableValue oauthSettingsserverUrlSettingsObserveValue = BeanProperties.value("oauthSettings.serverUrl").observe(settings);
		bindingContext.bindValue(observeTextTxtGenesysURLObserveWidget, oauthSettingsserverUrlSettingsObserveValue, null, null);
		//
		IObservableValue observeTextTxtAuthorizationEndpointObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtAuthorizationEndpoint);
		IObservableValue oauthSettingsauthorizationEndpointSettingsObserveValue = BeanProperties.value("oauthSettings.authorizationEndpoint").observe(settings);
		bindingContext.bindValue(observeTextTxtAuthorizationEndpointObserveWidget, oauthSettingsauthorizationEndpointSettingsObserveValue, null, null);
		//
		IObservableValue observeTextTxtTokenEndpointObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtTokenEndpoint);
		IObservableValue oauthSettingstokenEndpointSettingsObserveValue = BeanProperties.value("oauthSettings.tokenEndpoint").observe(settings);
		bindingContext.bindValue(observeTextTxtTokenEndpointObserveWidget, oauthSettingstokenEndpointSettingsObserveValue, null, null);
		//
		IObservableValue observeTextTxtApiUrlObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtApiUrl);
		IObservableValue oauthSettingsapiUrlSettingsObserveValue = BeanProperties.value("oauthSettings.apiUrl").observe(settings);
		bindingContext.bindValue(observeTextTxtApiUrlObserveWidget, oauthSettingsapiUrlSettingsObserveValue, null, null);
		//
		IObservableValue observeTextTxtAccessTokenObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtAccessToken);
		IObservableValue oauthSettingsaccessTokenSettingsObserveValue = BeanProperties.value("oauthSettings.accessToken").observe(settings);
		bindingContext.bindValue(observeTextTxtAccessTokenObserveWidget, oauthSettingsaccessTokenSettingsObserveValue, null, null);
		//
		IObservableValue observeTextTxtRefreshTokenObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtRefreshToken);
		IObservableValue oauthSettingsrefreshTokenSettingsObserveValue = BeanProperties.value("oauthSettings.refreshToken").observe(settings);
		bindingContext.bindValue(observeTextTxtRefreshTokenObserveWidget, oauthSettingsrefreshTokenSettingsObserveValue, null, null);
		//
		IObservableValue observeTextTxtClientKeyObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtClientKey);
		IObservableValue oauthSettingsclientKeySettingsObserveValue = BeanProperties.value("oauthSettings.clientKey").observe(settings);
		bindingContext.bindValue(observeTextTxtClientKeyObserveWidget, oauthSettingsclientKeySettingsObserveValue, null, null);
		//
		IObservableValue observeTextTxtClientSecretObserveWidget = WidgetProperties.text(SWT.Modify).observe(txtClientSecret);
		IObservableValue oauthSettingsclientSecretSettingsObserveValue = BeanProperties.value("oauthSettings.clientSecret").observe(settings);
		bindingContext.bindValue(observeTextTxtClientSecretObserveWidget, oauthSettingsclientSecretSettingsObserveValue, null, null);
		//
		return bindingContext;
	}
}
