package org.genesys2.anno.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.genesys2.anno.converter.GenesysJSONException;
import org.genesys2.anno.converter.GenesysJSONIncompleteException;
import org.genesys2.anno.converter.RowConverter;
import org.genesys2.anno.model.OAuthSettings;
import org.genesys2.anno.model.Settings;
import org.genesys2.anno.parser.RowReader;
import org.genesys2.client.oauth.GenesysApiException;
import org.genesys2.client.oauth.GenesysClient;
import org.genesys2.client.oauth.OAuthAuthenticationException;
import org.genesys2.client.oauth.PleaseRetryException;
import org.springframework.beans.factory.annotation.Autowired;

import swing2swt.layout.BorderLayout;

import com.fasterxml.jackson.databind.node.ObjectNode;

public class PushDialog extends Dialog {
	private static final Logger _log = Logger.getLogger(PushDialog.class);
	protected static final int BATCH_SIZE = 50;

	public static enum GenesysOp {
		UPSERT, DELETE
	}

	private static Image deleteDataIcon;
	private static Image parseDataIcon;

	@Autowired
	private Settings settings;

	@Autowired
	protected DataSourceLoader dataSourceLoader;

	BlockingQueue<Runnable> linkedBlockingDeque = new LinkedBlockingDeque<Runnable>(4);
	private ThreadPoolExecutor executorService = new ThreadPoolExecutor(1, 2, 30, TimeUnit.SECONDS, linkedBlockingDeque, new ThreadPoolExecutor.CallerRunsPolicy());

	protected Object result;
	protected Shell shell;
	private Text txtJson;
	private IDataSourceSheet dataSourceSheet;
	private ColumnDefs columnDefs;
	private SwtLogAppender logAppender;
	private IDataSource dataSource;

	static {
		deleteDataIcon = ImageDescriptor.createFromURL(PushDialog.class.getResource("/icon/delete.png")).createImage();
		parseDataIcon = ImageDescriptor.createFromURL(PushDialog.class.getResource("/icon/parse.png")).createImage();
	}

	/**
	 * Create the dialog.
	 * 
	 * @param parent
	 * @param style
	 */
	public PushDialog(Shell parent, int style) {
		super(parent, SWT.DIALOG_TRIM | SWT.MAX | SWT.RESIZE | SWT.APPLICATION_MODAL);
		setText("Push data to Genesys");
	}

	/**
	 * Open the dialog.
	 * 
	 * @return the result
	 */
	public Object open() {
		if (this.dataSourceSheet == null || this.columnDefs == null) {
			_log.warn("Can't open PushDialog, no columnDefs or no dataSourceSheet");
			return SWT.CANCEL;
		}

		createContents();

		logAppender = new SwtLogAppender(this.txtJson);
		logAppender.setConversionPattern("%d{yyyy-MM-dd HH:mm:ss} %-5p - %m%n");

		shell.open();
		shell.layout();

		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				_log.warn("Terminating thread pool!");
				executorService.shutdownNow();
				logAppender.close();
			}
		});
		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event event) {
				_log.info("Killing worker thread pool");
				executorService.shutdown();
				executorService.shutdownNow();
				event.doit = executorService.getActiveCount() == 0;
				_log.warn((executorService.getActiveCount()) + " jobs are still running, refusing to close.");
			}
		});
		shell.setSize(691, 383);
		shell.setText(getText());
		shell.setLayout(new BorderLayout(0, 0));

		ToolBar toolBar = new ToolBar(shell, SWT.FLAT);
		toolBar.setLayoutData(BorderLayout.NORTH);

		ToolItem tltmNewItem = new ToolItem(toolBar, SWT.NONE);
		tltmNewItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doTrialRun();
			}
		});
		tltmNewItem.setText("Parse all");
		tltmNewItem.setImage(PushDialog.parseDataIcon);

		ToolItem tltmPushToGenesys = new ToolItem(toolBar, SWT.NONE);
		tltmPushToGenesys.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (SwtUtil.showYesNoPrompt(getParent(), "Confirmation required", "Push data to Genesys server?\n" + settings.getOauthSettings().getServerUrl())) {
					_log.info("Upsert confirmed");
					doPush(GenesysOp.UPSERT);
				} else {
					_log.info("Upsert canceled");
				}
			}
		});
		tltmPushToGenesys.setText("Upload");
		tltmPushToGenesys.setImage(AppWindow.pushDataIcon);

		ToolItem tltmRemove = new ToolItem(toolBar, SWT.NONE);
		tltmRemove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if (SwtUtil.showYesNoPrompt(getParent(), "Confirmation required", "Delete data from Genesys server?\n" + settings.getOauthSettings().getServerUrl())) {
					_log.info("Delete confirmed");
					doPush(GenesysOp.DELETE);
				} else {
					_log.info("Delete canceled");
				}
			}
		});
		tltmRemove.setText("Remove");
		tltmRemove.setImage(PushDialog.deleteDataIcon);

		ToolItem tltmRadioItemDebug = new ToolItem(toolBar, SWT.RADIO);
		tltmRadioItemDebug.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				logAppender.setThreshold(Level.DEBUG);
			}
		});
		tltmRadioItemDebug.setSelection(true);
		tltmRadioItemDebug.setText("DEBUG");

		ToolItem tltmRadioItemInfo = new ToolItem(toolBar, SWT.RADIO);
		tltmRadioItemInfo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				logAppender.setThreshold(Level.INFO);
			}
		});
		tltmRadioItemInfo.setText("INFO");

		ToolItem tltmRadioItemWarning = new ToolItem(toolBar, SWT.RADIO);
		tltmRadioItemWarning.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				logAppender.setThreshold(Level.WARN);
			}
		});
		tltmRadioItemWarning.setText("WARNING");

		txtJson = new Text(shell, SWT.BORDER | SWT.READ_ONLY | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
		txtJson.setLayoutData(BorderLayout.CENTER);

	}

	protected void doTrialRun() {
		_log.info("Decoding data...");
		try {
			final RowReader rowReader = dataSourceLoader.createRowReader(dataSourceSheet, dataSource);
			rowReader.setSkipRows(dataSourceSheet.getHeaderRowIndex() + 1);

			executorService.execute(new Runnable() {
				@Override
				public void run() {
					int count = 0;
					try {
						do {
							List<Object[]> rows = rowReader.readRows(10);

							if (rows.size() == 0) {
								_log.info("Exhausted data source.");
								break;
							}

							if (count % 100 == 0)
								_log.debug("Examining data at row " + count);

							for (Object[] row : rows) {
								count++;
								try {
									Map<String, ?> accnMap = RowConverter.toMap(dataSourceSheet, row, columnDefs);
									ObjectNode accnJson = RowConverter.toJson(accnMap);
									final String instCode = accnJson.get("instCode").textValue();

								} catch (Throwable e) {
									_log.warn("Error in row " + count + ". " + e.getMessage() + ": " + ArrayUtils.toString(row));
								}
							}

							Thread.sleep(10);
						} while (true);

						_log.info("Done queuing parse jobs!");

					} catch (IOException e) {
						_log.error(e.getMessage(), e);
					} catch (InterruptedException e) {
						_log.info("Execution was interrupted");
					} finally {
						try {
							rowReader.close();
						} catch (IOException e) {
							_log.error("Failed to close rowReader: " + e.getMessage(), e);
						}
					}
				}
			});

		} catch (IOException e) {
			_log.error(e.getMessage(), e);
		} catch (UnsupportedDataFormatException e) {
			_log.error(e.getMessage(), e);
		} finally {
			_log.info("Done.");
		}
	}

	protected void doPush(final GenesysOp operation) {

		OAuthSettings oauthSettings = settings.getOauthSettings();
		_log.info("Starting push to " + oauthSettings.getServerUrl());
		final GenesysClient genesysClient = SpringConfig.createGenesysClient(oauthSettings);
		try {
			// Ping the server
			genesysClient.me();

			final RowReader rowReader = dataSourceLoader.createRowReader(dataSourceSheet, dataSource);
			rowReader.setSkipRows(dataSourceSheet.getHeaderRowIndex() + 1);

			executorService.execute(new Runnable() {
				final Map<String, List<ObjectNode>> instCodeMap = new HashMap<String, List<ObjectNode>>();

				@Override
				public void run() {
					int count = 0;
					try {

						do {
							List<Object[]> rows = rowReader.readRows(100);

							if (rows.size() == 0) {
								_log.info("Exhausted data source.");
								break;
							}

							_log.info("Batch start at row " + count);
							for (Object[] row : rows) {
								count++;
								_log.debug(count + ": " + ArrayUtils.toString(row));
								Map<String, ?> accnMap = RowConverter.toMap(dataSourceSheet, row, columnDefs);
								try {
									ObjectNode accnJson = RowConverter.toJson(accnMap);
									final String instCode = accnJson.get("instCode").textValue();

									List<ObjectNode> instCodeBatch = null;
									synchronized (instCodeMap) {
										instCodeBatch = instCodeMap.get(instCode);
										if (instCodeBatch == null) {
											instCodeMap.put(instCode, instCodeBatch = new ArrayList<ObjectNode>());
										}
									}

									synchronized (instCodeBatch) {
										instCodeBatch.add(accnJson);
										if (instCodeBatch.size() >= BATCH_SIZE) {
											doAsyncPush(operation, genesysClient, instCode, instCodeBatch);
										}
									}
								} catch (GenesysJSONIncompleteException e) {
									_log.warn("Ignoring incomplete accession " + ArrayUtils.toString(row));
								}

							}

							Thread.sleep(10);
						} while (true);

						_log.info("Pushing queued data");
						for (final String instCode : instCodeMap.keySet()) {
							List<ObjectNode> accns = instCodeMap.get(instCode);
							if (accns.size() > 0) {
								doAsyncPush(operation, genesysClient, instCode, accns);
							}
						}

						_log.info("Done queuing upload jobs!");

					} catch (IOException e) {
						_log.error(e.getMessage(), e);
					} catch (GenesysJSONException e) {
						_log.error("Genesys JSON conversion failed in batch" + count);
						_log.error(e.getMessage(), e);
					} catch (InterruptedException e) {
						_log.info("Execution was interrupted");
					} catch (Throwable e) {
						_log.error(e.getMessage(), e);
					} finally {
						_log.info("Reader finished.");
						try {
							rowReader.close();
						} catch (IOException e) {
							_log.error("Failed to close rowReader: " + e.getMessage(), e);
						}
					}
				}
			});

		} catch (OAuthAuthenticationException e) {
			_log.error(e.getMessage(), e);
		} catch (PleaseRetryException e) {
			_log.error(e.getMessage(), e);
		} catch (GenesysApiException e) {
			_log.error(e.getMessage(), e);
		} catch (IOException e) {
			_log.error(e.getMessage(), e);
		} catch (UnsupportedDataFormatException e) {
			_log.error(e.getMessage(), e);
		} catch (Throwable e) {
			_log.error(e.getMessage(), e);
		} finally {
			_log.info("Done.");
		}
	}

	public void setDataSourceSheet(IDataSourceSheet dss) {
		this.dataSourceSheet = dss;
	}

	public IDataSourceSheet getDataSourceSheet() {
		return dataSourceSheet;
	}

	public void setColumnDefs(ColumnDefs columnDefs) {
		this.columnDefs = columnDefs;
	}

	private void doAsyncPush(final GenesysOp operation, final GenesysClient genesysClient, final String instCode, final Collection<ObjectNode> instCodeBatch) {
		if (instCodeBatch.size() == 0) {
			_log.debug("Nothing to push");
			return;
		}

		executorService.execute(new Runnable() {
			final ArrayList<ObjectNode> accns = new ArrayList<ObjectNode>(instCodeBatch);
			final GenesysOp op = operation;

			@Override
			public void run() {
				try {
					_log.info("Pushing data for instCode=" + instCode + " size=" + accns.size());
					if (_log.isDebugEnabled()) {
						for (ObjectNode o : accns) {
							_log.debug(o);
						}
					}

					if (op == GenesysOp.UPSERT)
						genesysClient.updateAccessions(instCode, accns);
					else if (op == GenesysOp.DELETE)
						genesysClient.deleteAccessions(instCode, GenesysClient.toArrayNode(accns));

				} catch (OAuthAuthenticationException e) {
					_log.error(e.getMessage(), e);
				} catch (GenesysApiException e) {
					_log.error(e.getMessage(), e);
				} catch (InterruptedException e) {
					_log.info("Push was interrupted.");
				}
			}
		});

		instCodeBatch.clear();
	}

	public void setDataSource(IDataSource dataSource) {
		this.dataSource = dataSource;
	}
}
