/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.genesys2.anno.model.Column;
import org.genesys2.anno.model.ColumnDataType;
import org.genesys2.anno.model.DatabaseSettings;
import org.genesys2.anno.model.OAuthSettings;
import org.genesys2.anno.model.Settings;
import org.genesys2.anno.parser.CsvDataSourceSheet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;

public class DwcaBuilder extends AbstractModelObject {

    @Autowired
    private DataSourceLoader dataSourceLoader;

	private List<IDataSource> fileRoots = new ArrayList<IDataSource>();
	private String workspaceFilename = null;

	public List<IDataSource> getFileRoots() {
		return fileRoots;
	}

	public String getWorkspaceFilename() {
		return workspaceFilename;
	}

	public void addDataSource(IDataSource dataSource) {
		if (dataSource == null)
			return;
		fileRoots.add(dataSource);
		firePropertyChange("fileRoots", null, this);
	}

	public void removeDataSource(IDataSource dataSource) {
		fileRoots.remove(dataSource);
		firePropertyChange("fileRoots", null, this);
	}

	public void saveWorkspace(String filePath, Settings sourceSettings) throws JSONException, IOException {
		File workspaceFile = new File(filePath);
		if (!workspaceFile.exists()) {
			workspaceFile.createNewFile();
			FileWriter fileWriter = new FileWriter(filePath);
			fileWriter.write("{}");
			fileWriter.flush();
			fileWriter.close();
		}

		FileReader reader = new FileReader(filePath);
		JSONTokener tokener = new JSONTokener(reader);
		JSONObject main = new JSONObject(tokener);
		JSONArray files = new JSONArray();

		JSONObject oauthSettings = new JSONObject();
		oauthSettings.put("accessToken", sourceSettings.getOauthSettings().getAccessToken());
		oauthSettings.put("apiUrl", sourceSettings.getOauthSettings().getApiUrl());
		oauthSettings.put("authorizationEndpoint", sourceSettings.getOauthSettings().getAuthorizationEndpoint());
		oauthSettings.put("clientKey", sourceSettings.getOauthSettings().getClientKey());
		oauthSettings.put("clientSecret", sourceSettings.getOauthSettings().getClientSecret());
		oauthSettings.put("refreshToken", sourceSettings.getOauthSettings().getRefreshToken());
		oauthSettings.put("serverUrl", sourceSettings.getOauthSettings().getServerUrl());
		oauthSettings.put("tokenEndpoint", sourceSettings.getOauthSettings().getTokenEndpoint());

		JSONObject settings = new JSONObject();
		settings.put("oauthSettings", oauthSettings);

		for (IDataSource sourceFile : getFileRoots()) {
			JSONObject file = new JSONObject();
			JSONArray sheets = new JSONArray();

			if (sourceFile instanceof DataSourceFile) {
				String path = sourceFile.getFile().getPath();
				file.put("path", path);
			} else if (sourceFile instanceof JdbcDataSource) {
				JSONObject dataBaseSettings = new JSONObject();
				dataBaseSettings.put("url", ((JdbcDataSource) sourceFile).getSettings().getUrl());
				dataBaseSettings.put("user", ((JdbcDataSource) sourceFile).getSettings().getUser());
				dataBaseSettings.put("password", ((JdbcDataSource) sourceFile).getSettings().getPassword());
				dataBaseSettings.put("driver", ((JdbcDataSource) sourceFile).getSettings().getDriverClassName());

				file.put("dataBaseSettings", dataBaseSettings);
				file.put("name", sourceFile.getFileName());
			}

			file.put("sheets", sheets);
			files.put(file);

			for (IDataSourceSheet sourceSheet : sourceFile.getSheets()) {
				JSONObject sheet = new JSONObject();
				JSONArray columns = new JSONArray();

				String name = sourceSheet.getSheetName();

				sheet.put("name", name);
				sheet.put("columns", columns);
				sheet.put("headers", sourceSheet.isHeadersIncluded());
				sheet.put("headerRow", sourceSheet.getHeaderRowIndex());

				if (sourceSheet instanceof CsvDataSourceSheet) {
					CsvDataSourceSheet csvSheet = (CsvDataSourceSheet) sourceSheet;
					JSONObject csv = new JSONObject();
					csv.put("charset", csvSheet.getCharset());
					csv.put("delimiterChar", "" + csvSheet.getDelimiterChar());
					csv.put("quoteChar", "" + csvSheet.getQuoteChar());
					csv.put("unixEol", csvSheet.isUnixEol());
					sheet.put("csv", csv);
				}
				if (sourceSheet instanceof JdbcDataSourceSheet) {
					sheet.put("query", sourceSheet.getQuery());
				}

				sheets.put(sheet);

				for (Column sourceColumn : sourceSheet.getColumns()) {

					JSONObject column = new JSONObject();
					sourceColumn.setDataType(ColumnDataType.TEXT);
					column.put("name", sourceColumn.getPreferredName());
					column.put("rdfTerm", sourceColumn.getRdfTerm());
					column.put("description", sourceColumn.getDescription());
					column.put("multiple", sourceColumn.isMultiple());
					column.put("dataType", sourceColumn.getDataType());
					column.put("pattern", sourceColumn.getPattern());
					column.put("separator", sourceColumn.getSeparator());
					column.put("groupPattern", sourceColumn.getGroupPattern());
					columns.put(column);
				}
			}
		}

		main.put("settings", settings);
		main.put("files", files);
		main.put("dateCreated", System.currentTimeMillis());
		main.put("version", 1);

		FileWriter fileWriter = new FileWriter(filePath);
		fileWriter.write(main.toString());
		fileWriter.flush();
		fileWriter.close();

		System.out.println(main.toString());
		this.workspaceFilename = filePath;
	}

	public void loadWorkspace(String filePath, Settings settings) throws FileNotFoundException, JSONException {
		fileRoots.clear();

		FileReader reader = new FileReader(filePath);
		JSONTokener tokener = new JSONTokener(reader);
		JSONObject root = new JSONObject(tokener);

		if (root.has("files")) {
			JSONArray files = root.getJSONArray("files");
			for (int i = 0; i < files.length(); i++) {
				JSONObject sourceJ = files.getJSONObject(i);

				IDataSource sourceFile = null;
				File file = null;
				if (sourceJ.has("path")) {
					String path = sourceJ.getString("path");
					file = new File(path);
					sourceFile = new DataSourceFile();
					sourceFile.setFile(file);
				} else {
					sourceFile = new JdbcDataSource(sourceJ.getString("name"));
					JSONObject dataBaseSettingsJ = sourceJ.getJSONObject("dataBaseSettings");
					DatabaseSettings databaseSettings = new DatabaseSettings();
					databaseSettings.setUrl(dataBaseSettingsJ.getString("url"));
					databaseSettings.setUser(dataBaseSettingsJ.getString("user"));
					databaseSettings.setPassword(dataBaseSettingsJ.getString("password"));
					databaseSettings.setDriverClassName(dataBaseSettingsJ.getString("driver"));
					((JdbcDataSource) sourceFile).setSettings(databaseSettings);
				}

				JSONArray sheets = sourceJ.getJSONArray("sheets");
				List<IDataSourceSheet> sourceSheetList = new ArrayList<IDataSourceSheet>();

				for (int j = 0; j < sheets.length(); j++) {
					JSONObject sheetJ = sheets.getJSONObject(j);
					IDataSourceSheet sourceSheet = null;
					if (sheetJ.has("csv")) {
						CsvDataSourceSheet csvSourceSheet = new CsvDataSourceSheet(file);
						JSONObject csvJ = sheetJ.getJSONObject("csv");
						csvSourceSheet.setCharset(csvJ.getString("charset"));
						csvSourceSheet.setDelimiterChar(csvJ.getString("delimiterChar").charAt(0));
						csvSourceSheet.setQuoteChar(csvJ.getString("quoteChar").charAt(0));
						csvSourceSheet.setUnixEol(csvJ.getBoolean("unixEol"));
						sourceSheet = csvSourceSheet;
					} else if (!sourceJ.has("path")) {
						sourceSheet = new JdbcDataSourceSheet();
						if (sheetJ.has("query")) {
							sourceSheet.setQuery(sheetJ.getString("query"));
						}
						if (sheetJ.has("name")) {
							sourceSheet.setSheetName(sheetJ.getString("name"));
						}

					} else {
						sourceSheet = new DataSourceSheet(file);
					}
					sourceSheet.setSheetName(sheetJ.getString("name"));

					JSONArray columns = sheetJ.getJSONArray("columns");
					List<Column> columnList = new ArrayList<Column>();

                    List<Object[]> rows=null;
                    try {
                        rows = dataSourceLoader.loadRows(sourceSheet, 200);
                        sourceSheet.updateData(rows);
                    } catch (NullPointerException e){
                        //ignore
                    } catch (UnsupportedDataFormatException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    List<Column> loadedColumns = sourceSheet.getColumns();

                    for (int x = 0; x < columns.length(); x++) {
                        JSONObject column = columns.getJSONObject(x);

                        Column sourceColumn = new Column();

                        if (column.has("name"))
                            sourceColumn.setPreferredName(column.getString("name"));
                        if (column.has("rdfTerm"))
                            sourceColumn.setRdfTerm(column.getString("rdfTerm"));
                        if (column.has("description"))
                            sourceColumn.setDescription(column.getString("description"));
                        if (column.has("pattern"))
                            sourceColumn.setPattern(column.getString("pattern"));
                        if (column.has("separator"))
                            sourceColumn.setSeparator(column.getString("separator"));
                        if (column.has("groupPattern"))
                            sourceColumn.setGroupPattern(column.getString("groupPattern"));
                        if (column.has("dataType"))
                            sourceColumn.setDataType(ColumnDataType.valueOf(column.getString("dataType")));
                        if (column.has("multiple"))
                            sourceColumn.setMultiple(column.getBoolean("multiple"));

                        if (loadedColumns.contains(sourceColumn)){
                            columnList.add(sourceColumn);
                        }

                    }
                    for (Column loadedColumn:loadedColumns){

                        if (!columnList.contains(loadedColumn)){
                            columnList.add(loadedColumns.indexOf(loadedColumn),loadedColumn);
                        }
                    }
                    sourceSheet.setColumns(columnList);
                    sourceSheetList.add(sourceSheet);
                }

				sourceFile.setSheets(sourceSheetList);

				addDataSource(sourceFile);
			}
		}
		if (root.has("settings")) {
			JSONObject jsonSettings = root.getJSONObject("settings").getJSONObject("oauthSettings");

			OAuthSettings oAuthSettings = new OAuthSettings();
			oAuthSettings.setAccessToken(jsonSettings.getString("accessToken"));
			oAuthSettings.setApiUrl(jsonSettings.getString("apiUrl"));
			oAuthSettings.setAuthorizationEndpoint(jsonSettings.getString("authorizationEndpoint"));
			oAuthSettings.setClientKey(jsonSettings.getString("clientKey"));
			oAuthSettings.setClientSecret(jsonSettings.getString("clientSecret"));
			oAuthSettings.setRefreshToken(jsonSettings.getString("refreshToken"));
			oAuthSettings.setServerUrl(jsonSettings.getString("serverUrl"));
			oAuthSettings.setTokenEndpoint(jsonSettings.getString("tokenEndpoint"));

			settings.setOauthSettings(oAuthSettings);
		}

		this.workspaceFilename = filePath;
	}

}
