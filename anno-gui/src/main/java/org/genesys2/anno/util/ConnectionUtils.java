package org.genesys2.anno.util;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

import org.genesys2.anno.gui.ExtraClassLoader;
import org.springframework.beans.factory.annotation.Autowired;

public class ConnectionUtils {

	private static ConnectionUtils _instance;

	@Autowired
	ExtraClassLoader jdbcDriverClassLoader;

	public ConnectionUtils() {
		_instance = this;
	}

	public static Connection getConnection(String driverClassName, String url, String user, String password) throws ClassNotFoundException, SQLException {
		Connection con = null;
		try {
			con = DriverManager.getConnection(url, user, password);
			con.setReadOnly(true);
			return con;
		} catch (Throwable e) {
			System.err.println(e.getClass() + ": " + e.getMessage());
			e.printStackTrace();
			// Try the rest
		}

		try {
			String driver = driverClassName;

			Driver d = (Driver) Class.forName(driver, true, _instance.jdbcDriverClassLoader).newInstance();
			System.out.println("Instance of " + d.getClass() + " created!");
			DriverManager.registerDriver(new DriverShim(d));
			System.out.println("Driver registered");

			con = DriverManager.getConnection(url, user, password);
			con.setReadOnly(true);

			System.out.println("Got connection " + con);
		} catch (ClassNotFoundException cnfe) {
			throw cnfe;
		} catch (SQLException sqe) {
			throw sqe;
		} catch (InstantiationException e) {
			throw new ClassNotFoundException(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			throw new ClassNotFoundException(e.getMessage(), e);
		}
		return con;

	}

	public static void close(Connection connection) {
		try {
			if (connection != null && !connection.isClosed()) {
				connection.close();
			}
		} catch (SQLException ignored) {
		}
	}

	public static class DriverShim implements Driver {

		private Driver driver;

		public DriverShim(Driver d) {
			this.driver = d;
		}

		@Override
		public boolean acceptsURL(String url) throws SQLException {
			return driver.acceptsURL(url);
		}

		@Override
		public Connection connect(String url, Properties info) throws SQLException {
			return driver.connect(url, info);
		}

		@Override
		public int getMajorVersion() {
			return driver.getMajorVersion();
		}

		@Override
		public int getMinorVersion() {
			return driver.getMinorVersion();
		}

		@Override
		public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
			return driver.getPropertyInfo(url, info);
		}

		@Override
		public boolean jdbcCompliant() {
			return driver.jdbcCompliant();
		}

		public Logger getParentLogger() throws SQLFeatureNotSupportedException {
			throw new SQLFeatureNotSupportedException("getParentLogger() is Java 7");
		}
	}
}
