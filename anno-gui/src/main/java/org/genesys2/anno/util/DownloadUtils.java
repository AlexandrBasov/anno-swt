package org.genesys2.anno.util;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.genesys2.anno.gui.SwtUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadUtils {
	private static final Logger _log = Logger.getLogger(DownloadUtils.class);

	private static final int BUFFER_SIZE = 4096;

	public static void downloadFile(String fileURL, String saveDir)
            throws IOException {
        URL url = new URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");

            if (disposition != null) {

                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {

                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                        fileURL.length());
            }

            _log.info("FileName = " + fileName);


            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = saveDir + File.separator + fileName;


            FileOutputStream outputStream = new FileOutputStream(saveFilePath);
            
            IOUtils.copy(inputStream, outputStream);
            
            outputStream.close();
            inputStream.close();

            _log.info("File downloaded");
        } else {
            _log.error("No file to download. Server replied HTTP code: " + responseCode);
        } 
        httpConn.disconnect();
    }
}