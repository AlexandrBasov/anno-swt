/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/* ====================================================================
 Licensed to the Apache Software Foundation (ASF) under one or more
 contributor license agreements.  See the NOTICE file distributed with
 this work for additional information regarding copyright ownership.
 The ASF licenses this file to You under the Apache License, Version 2.0
 (the "License"); you may not use this file except in compliance with
 the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ==================================================================== */

package org.genesys2.anno.reader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * File for HSSF testing/examples
 * 
 * THIS IS NOT THE MAIN HSSF FILE!! This is a utility for testing functionality.
 * It does contain sample API usage that may be educational to regular API
 * users.
 * 
 * @see #main
 * @author Andrew Oliver (acoliver at apache dot org)
 */
public final class XSSFReadWrite {
	private static final Logger _log = Logger.getLogger(XSSFReadWrite.class);

	/**
	 * creates an {@link HSSFWorkbook} the specified OS filename.
	 * 
	 * @throws InvalidFormatException
	 */
	private static Workbook readFile(String filename) throws IOException, InvalidFormatException {
		return WorkbookFactory.create(new File(filename));
	}

	public static void dumpFile(String fileName) throws IOException {
		Workbook wb;
		try {
			wb = XSSFReadWrite.readFile(fileName);
		} catch (InvalidFormatException e) {
			throw new IOException(e.getMessage(), e);
		}

		_log.debug("Data dump:\n");

		for (int k = 0; k < wb.getNumberOfSheets(); k++) {
			Sheet sheet = wb.getSheetAt(k);
			int rows = sheet.getPhysicalNumberOfRows();
			_log.debug("Sheet " + k + " \"" + wb.getSheetName(k) + "\" has " + rows + " row(s).");
			for (int r = 0; r < rows; r++) {
				Row row = sheet.getRow(r);
				if (row == null) {
					continue;
				}

				int cells = row.getPhysicalNumberOfCells();
				_log.debug("\nROW " + row.getRowNum() + " has " + cells + " cell(s).");
				for (int c = 0; c < cells; c++) {
					Cell cell = row.getCell(c);
					String value = null;

					switch (cell.getCellType()) {

					case Cell.CELL_TYPE_FORMULA:
						value = "FORMULA value=" + cell.getCellFormula();
						break;

					case Cell.CELL_TYPE_NUMERIC:
						value = "NUMERIC value=" + cell.getNumericCellValue();
						break;

					case Cell.CELL_TYPE_STRING:
						value = "STRING value=" + cell.getStringCellValue();
						break;

					default:
					}
					_log.debug("CELL col=" + cell.getColumnIndex() + " VALUE=" + value);
				}
			}
		}
	}

	public static void processAllSheets(String filename) throws Exception {
		OPCPackage pkg = OPCPackage.open(filename, PackageAccess.READ);
		try {
			XSSFReader r = new XSSFReader(pkg);
			SharedStringsTable sst = r.getSharedStringsTable();

			XMLReader parser = fetchSheetParser(sst);

			Iterator<InputStream> sheets = r.getSheetsData();
			while (sheets.hasNext()) {
				_log.debug("Processing new sheet:\n");
				InputStream sheet = sheets.next();
				try {
					InputSource sheetSource = new InputSource(sheet);
					parser.parse(sheetSource);
				} finally {
					_log.debug("Closing sheet");
					sheet.close();
					_log.debug("");
				}
			}

		} finally {
			_log.debug("Closing OPCPackage");
			pkg.revert();
			pkg.close();
		}
	}

	public static XMLReader fetchSheetParser(SharedStringsTable sst) throws SAXException {
		XMLReader parser = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
		ContentHandler handler = new SheetHandler(sst);
		parser.setContentHandler(handler);
		return parser;
	}

	/**
	 * See org.xml.sax.helpers.DefaultHandler javadocs
	 */
	private static class SheetHandler extends DefaultHandler {
		private SharedStringsTable sst;
		private String lastContents;
		private boolean nextIsString;
		private int currentRow = 0;

		private SheetHandler(SharedStringsTable sst) {
			this.sst = sst;
		}

		@Override
		public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
			// _log.debug(">> " + name);
			// row => row
			if (name.equals("row")) {
				currentRow++;
				if (currentRow == 100) {
					throw new StopParsingException("Stopping parse after " + currentRow + " rows");
				}
				_log.debug("NEW ROW------- " + currentRow);
			}
			// c => cell
			else if (name.equals("c")) {
				// Print the cell reference
				System.out.print(attributes.getValue("r") + " - ");

				// for (int i=attributes.getLength()-1; i>=0; i--)
				// _log.debug(attributes.getLocalName(i) + "=" +
				// attributes.getValue(i));

				// Figure out if the value is an index in the SST
				String cellType = attributes.getValue("t");
				if (cellType != null && cellType.equals("s")) {
					nextIsString = true;
				} else {
					nextIsString = false;
				}
			}
			// Clear contents cache
			lastContents = "";
		}

		@Override
		public void endElement(String uri, String localName, String name) throws SAXException {
			// Process the last contents as required.
			// Do now, as characters() may be called more than once
			if (nextIsString) {
				int idx = Integer.parseInt(lastContents);
				lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
				nextIsString = false;
			}

			// v => contents of a cell
			// Output after we've seen the string contents
			if (name.equals("v")) {
				_log.debug(lastContents);
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			lastContents += new String(ch, start, length);
		}
	}
}
