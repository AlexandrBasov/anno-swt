package org.genesys2.anno.reader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.genesys2.anno.model.DatabaseSettings;
import org.genesys2.anno.parser.JdbcRowReader;
import org.genesys2.anno.parser.RowReader;
import org.genesys2.anno.util.ConnectionUtils;

public class JDBCRowReader {
	private static final Logger _log = Logger.getLogger(JDBCRowReader.class);

	public List<Object[]> getRows(DatabaseSettings databaseSettings,String query, int limit) throws SQLException, ClassNotFoundException {

        String url = databaseSettings.getUrl();
        String user = databaseSettings.getUser();
        String password = databaseSettings.getPassword();
        String driverClassName = databaseSettings.getDriverClassName();

        List<Object[]> rows = new ArrayList<Object[]>();

		Connection conn = ConnectionUtils.getConnection(driverClassName,url, user, password);

		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setMaxRows(limit);

			ResultSet res = ps.executeQuery();
			ResultSetMetaData metaData = res.getMetaData();

			int columnCount = metaData.getColumnCount();
			ArrayList<String> columns = new ArrayList<String>();
			for (int i = 1; i <= columnCount; i++) {
				columns.add(metaData.getColumnLabel(i));
			}
			rows.add(columns.toArray());
			while (res.next()) {
				List<Object> row = new ArrayList<Object>();
				for (int i = 1; i <= columnCount; i++) {
					Object val = res.getObject(i);
					row.add(val);
				}
				rows.add(row.toArray());
			}
			return rows;
		} finally {
			ConnectionUtils.close(conn);
		}
	}

	public RowReader createRowReader(String query,DatabaseSettings databaseSettings) {
		return new JdbcRowReader(query,databaseSettings);
	}
}
