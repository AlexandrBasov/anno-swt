package org.genesys2.anno.converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.genesys2.anno.gui.ColumnDefs;
import org.genesys2.anno.gui.IDataSourceSheet;
import org.genesys2.anno.model.Column;
import org.genesys2.anno.model.ColumnDef;
import org.genesys2.anno.predefined.GenesysJSON;
import org.genesys2.anno.predefined.GenesysJSON.JsonField;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

public class RowConverter {
	private static final Logger _log = Logger.getLogger(RowConverter.class);

	private static final ObjectMapper mapper = new ObjectMapper();
	private static final GenesysJSON genesysJson = new GenesysJSON();

	public static Map<String, ?> toMap(IDataSourceSheet dataSourceSheet, Object[] row, ColumnDefs columnDefs) {
		HashMap<String, Object> map = new HashMap<String, Object>(columnDefs.getColumnDefs().size());

		_log.debug("Converting " + ArrayUtils.toString(row));

		List<Column> columns = dataSourceSheet.getColumns();
		for (int i = 0; i < columns.size(); i++) {

			Column column = columns.get(i);
			ColumnDef columnDef = findColumnDef(column, columnDefs.getColumnDefs());
			if (columnDef == null) {
				continue;
			}

			if (i >= row.length) {
				if (column.isIncludeNull()) {
					map.put(columnDef.getRdfTerm(), null);
				}
				_log.debug("Row has less than " + (i + 1) + " columns: len=" + row.length + " " + ArrayUtils.toString(row));
				continue;
			}


			Object rowValue = convertCellValue(row[i], column.isMultiple(), column.getSeparator(), column.getPattern(), column.getGroupPattern());

			Object mapValue = map.get(columnDef.getRdfTerm());
			if (mapValue == null) {
				_log.debug("Adding value for " + columnDef.getRdfTerm() + " val=" + rowValue);
				if (rowValue instanceof String[]) {
					_log.debug("val=" + ArrayUtils.toString(rowValue));
					map.put(columnDef.getRdfTerm(), new ArrayList<String>(Arrays.asList((String[]) rowValue)));
				} else {
					map.put(columnDef.getRdfTerm(), rowValue);
				}
			} else if (mapValue instanceof List<?>) {
				_log.debug("Adding " + columnDef.getRdfTerm() + " to List<?>" + " val=" + rowValue);
				if (rowValue instanceof String[]) {
					((List<Object>) mapValue).addAll(Arrays.asList((String[]) rowValue));
				} else if (rowValue != null) {
					((List<Object>) mapValue).add(rowValue);
				}
			} else if (rowValue != null) {
				_log.debug("Converting " + columnDef.getRdfTerm() + " to List<?>" + " val=" + rowValue);
				Object currentVal = mapValue;
				List<Object> newVal = new ArrayList<Object>();
				newVal.add(currentVal);
				if (rowValue instanceof String[]) {
					newVal.addAll(Arrays.asList((String[]) rowValue));
				} else {
					newVal.add(rowValue);
				}
				map.put(columnDef.getRdfTerm(), newVal);
			} else if (rowValue == null) {
				if (column.isIncludeNull()) {
					map.put(columnDef.getRdfTerm(), null);
				}
			}
		}

		// try {
		// StringWriter out = new StringWriter();
		// mapper.writeValue(out, map);
		// _log.info(out.getBuffer().toString());
		// } catch (Throwable e) {
		// _log.error(e.getMessage(), e);
		// }

		return map;
	}

	private static Object convertCellValue(Object value, boolean splitString, String separator, String pattern, String groupPattern) {
		if (value != null) {
			if (value instanceof String) {
				String stringValue = ((String) value);
				if (StringUtils.isBlank(stringValue)) {
					return null;
				}
				if (splitString) {
					String[] split = stringValue.split(separator);
					for (int i = split.length - 1; i >= 0; i--) {
						if (pattern != null) {
							split[i] = valueFromPattern(split[i].trim(), pattern, groupPattern);
						} else {
							split[i] = split[i].trim();
						}
					}
					return split;
				}

				String patVal = valueFromPattern(stringValue.trim(), pattern, groupPattern);
				if (StringUtils.isBlank(patVal)) {
					return null;
				}
				return patVal;
			}
		}

		// IGNORE?
		return value;
	}

	private static String valueFromPattern(String trim, String pattern, String groupPattern) {
		if (StringUtils.isBlank(pattern)) {
			return trim;
		}
		Pattern pat = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pat.matcher(trim);
		if (matcher.find()) {
			String res = matcher.groupCount() == 0 ? matcher.group() : matcher.group(1);
			if (StringUtils.isNotBlank(groupPattern)) {
				res = groupPattern;
				if (res.contains("$1"))
					res = res.replaceAll("\\$1", matcher.group(1));
				if (res.contains("$2"))
					res = res.replaceAll("\\$2", matcher.group(2));
			}
			// System.err.println("Pattern=" + pattern + " groups=" +
			// matcher.groupCount() + " res=" + res);
			// throw new RuntimeException("foobar");
			return res;
		}
		return null;
	}

	private static ColumnDef findColumnDef(Column column, List<ColumnDef> columnDefList) {
		for (ColumnDef columnDef : columnDefList) {
			if (columnDef.getRdfTerm().equals(column.getRdfTerm())) {
				return columnDef;
			}
		}
		// _log.trace("ColumnDef not found for column=" +
		// column.getPreferredName() + " term=" + column.getRdfTerm());
		return null;
	}

	public static List<Map<String, ?>> toMap(IDataSourceSheet dataSourceSheet, List<Object[]> rows, ColumnDefs columnDefs) {
		List<Map<String, ?>> list = new ArrayList<Map<String, ?>>(rows.size());
		for (Object[] row : rows) {
			list.add(toMap(dataSourceSheet, row, columnDefs));
		}
		return list;
	}

	public static ArrayNode toGenesysJson(List<Map<String, ?>> list) throws GenesysJSONException, JsonGenerationException {
		ArrayNode result = mapper.createArrayNode();

		for (Map<String, ?> row : list) {

			ObjectNode rowObject = toJson(row);
			if (rowObject != null) {
				result.add(rowObject);
			}
		}

		return result;
	}

	public static ObjectNode toJson(Map<String, ?> row) throws GenesysJSONException, JsonGenerationException {
		if (row == null)
			return null;

		ObjectNode rowNode = mapper.createObjectNode();

		for (GenesysJSON.JsonField jsonField : genesysJson.getJsonFields()) {
			if (!row.containsKey(jsonField.getRdfTerm()))
				continue;
			Object rdfValues = row.get(jsonField.getRdfTerm());
			add(rowNode, jsonField, rdfValues);
		}

		for (GenesysJSON.JsonField jsonField : genesysJson.getRequiredFields()) {
			if (!hasNodeValue(rowNode, jsonField.getFieldName())) {
				throw new GenesysJSONIncompleteException("Missing required property '" + jsonField.getFieldName() + "' " + jsonField.getRdfTerm());
			}
		}

		return rowNode;
	}

	private static void add(ObjectNode rowNode, JsonField jsonField, Object rdfValues) throws GenesysJSONException, JsonGenerationException {
		String fieldName = jsonField.getFieldName();
		ObjectNode containerNode = ensureNode(rowNode, fieldName);
		int dotPost = fieldName.lastIndexOf('.');
		if (dotPost > -1)
			fieldName = fieldName.substring(dotPost + 1);
		// System.err.println(">> " + jsonField.getFieldName() + " field=" +
		// fieldName);
		JsonNode valueNode = containerNode.get(fieldName);

		if (valueNode == null) {
			if (jsonField.hasAllowMultiple()) {
				valueNode = containerNode.putArray(fieldName);
			}
		} else {
			if (jsonField.hasAllowMultiple() && !valueNode.isArray()) {
				throw new GenesysJSONException("Expecting arrayObject for " + fieldName);
			}
		}

		if (rdfValues == null) {
			containerNode.set(fieldName, null);
		} else if (rdfValues instanceof ArrayList<?>) {
			ArrayNode arrayNode = (ArrayNode) valueNode;
			for (String rdfValue : (ArrayList<String>) rdfValues) {
				arrayNode.add(coerce(jsonField.getType(), rdfValue));
			}
		} else {
			if (jsonField.hasAllowMultiple()) {
				ArrayNode arrayNode = (ArrayNode) valueNode;
				arrayNode.add(coerce(jsonField.getType(), rdfValues));
			} else {
				// System.err.println("Putting field=" + fieldName + " type=" +
				// rdfValues.getClass());
				containerNode.set(fieldName, coerce(jsonField.getType(), rdfValues));
			}
		}
	}

	static ObjectNode ensureNode(ObjectNode parentNode, String nodeName) throws JsonGenerationException {
		if (StringUtils.isBlank(nodeName)) {
			throw new JsonGenerationException("nodeName should not be blank or null");
		}
		int dotIndex = nodeName.indexOf('.');
		if (dotIndex > -1) {
			String[] path = nodeName.split("\\.");
			for (int i = 0; i < path.length - 1; i++) {
				JsonNode node = parentNode.get(path[i]);
				if (node == null) {
					node = parentNode.putObject(path[i]);
				}
				parentNode = (ObjectNode) node;
			}
		}
		return parentNode;
	}

	static boolean hasNodeValue(final ObjectNode parentNode, final String nodeName) throws JsonGenerationException {
		if (StringUtils.isBlank(nodeName)) {
			throw new JsonGenerationException("nodeName should not be blank or null");
		}
		String[] path = nodeName.split("\\.");
		JsonNode node = parentNode;
		for (int i = 0; i < path.length; i++) {
			node = node.get(path[i]);
			if (node == null) {
				return false;
			}
		}
		// Array nodes should have a non-null value
		if (node != null && node.isArray()) {
			Iterator<JsonNode> it = node.elements();
			while (it.hasNext()) {
				JsonNode arrVal = it.next();
				if (arrVal != null && !arrVal.isNull()) {
					return true;
				}
			}
		}
		return node != null && !node.isNull() && !node.isMissingNode() && !node.isArray() && node.isValueNode();
	}

	static JsonNode coerce(Class<?> clazz, Object rdfValues) {
		if (rdfValues == null) {
			return null;
		}
		if (clazz == String.class) {
			return new TextNode(rdfValues.toString());
		}
		if (clazz == double.class || clazz == float.class) {
			return new DoubleNode(Double.parseDouble(rdfValues.toString()));
		}
		if (clazz == int.class || clazz == long.class) {
			return new LongNode(Long.parseLong(rdfValues.toString()));
		}
		if (clazz == boolean.class) {
			return BooleanNode.valueOf(toBoolean(rdfValues.toString()));
		}
		throw new RuntimeException("Unsupported JSON type " + clazz);
	}

	private static boolean toBoolean(String rdfValue) {
		return "true".equalsIgnoreCase(rdfValue) || "yes".equalsIgnoreCase(rdfValue) || "t".equalsIgnoreCase(rdfValue) || "y".equalsIgnoreCase(rdfValue) || "1".equalsIgnoreCase(rdfValue);
	}

}
