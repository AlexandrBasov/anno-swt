package org.genesys2.anno.parser;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFReader.SheetIterator;
import org.apache.poi.xssf.model.StylesTable;
import org.genesys2.anno.reader.MyXSSFSheetHandler;
import org.genesys2.anno.reader.MyXSSFSheetHandler.RowHandler;
import org.genesys2.anno.reader.StopParsingException;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class XlsxRowReader implements RowReader {
	private static final int MAX_XLSXREADER_BUFFER = 10000;

	private static final Logger _log = Logger.getLogger(XlsxRowReader.class);

	private Thread thread;

	private XlsxReader threadedReader;

	private Queue<Object[]> rowsQueue = new LinkedBlockingQueue<Object[]>(MAX_XLSXREADER_BUFFER);

	public XlsxRowReader(File sourceFile, String sheetName) {
		if (sheetName == null) {
			throw new IllegalArgumentException("sheetName cannot be null");
		}

		if (_log.isDebugEnabled()) {
			_log.debug("Looking for sheet=" + sheetName + " in file=" + sourceFile);
		}

		OPCPackage pkg;
		try {
			// pkg is passed to threadedReader where it is also closed
			pkg = OPCPackage.open(sourceFile, PackageAccess.READ);

		} catch (InvalidFormatException e) {
			throw new RuntimeException("Could not open " + sourceFile.getAbsolutePath(), e);
		}

		try {
			XSSFReader r = new XSSFReader(pkg);
			ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(pkg);
			StylesTable styles = r.getStylesTable();

			XSSFReader.SheetIterator sheetsIterator = (SheetIterator) r.getSheetsData();
			while (sheetsIterator.hasNext()) {
				InputStream sheet = sheetsIterator.next();
				_log.debug("Found sheet: '" + sheetsIterator.getSheetName() + "'");

				if (sheetName.equals(sheetsIterator.getSheetName())) {
					_log.debug("Found sheet.");

					this.threadedReader = new XlsxReader(pkg, styles, strings, sheet, this.rowsQueue);
					this.thread = new Thread(threadedReader, "xlsx-reader-" + System.currentTimeMillis());
					this.thread.start();
					_log.info("Background reader thread started threadName=" + this.thread.getName());

					// Ready!
					return;
				}
			}

		} catch (IOException e1) {
			throw new RuntimeException(e1);
		} catch (OpenXML4JException e1) {
			throw new RuntimeException(e1);
		} catch (SAXException e1) {
			throw new RuntimeException(e1);
		} finally {

		}

		throw new RuntimeException("Could not get reader");
	}

	@Override
	public List<Object[]> readRows(int rowsToRead) throws IOException {
		if (_log.isDebugEnabled()) {
			_log.debug("Reading from XLSX stream rowsToRead=" + rowsToRead);
		}

		List<Object[]> readRows = new ArrayList<Object[]>(rowsToRead);
		for (; rowsToRead > 0; rowsToRead--) {
			while (rowsQueue.size() == 0) {
				_log.debug("Queue is empty.");
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}

				if (rowsQueue.size() == 0 && (!this.thread.isAlive() || this.thread.isInterrupted())) {
					_log.debug("Reader thread is no longer alive!");
					return readRows;
				}
			}
			readRows.add(rowsQueue.poll());
		}

		return readRows;
	}

	@Override
	public void setSkipRows(int skipRows) {
		threadedReader.setSkipRows(skipRows);
	}

	@Override
	public void close() throws IOException {
		// Interrupt the Xlsx parser thread
		this.thread.interrupt();

		// Join the thread
		try {
			this.thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		this.thread = null;
		this.threadedReader.close();
	}

	private static XMLReader fetchSheetParser(ContentHandler handler) throws SAXException {
		XMLReader parser = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
		parser.setContentHandler(handler);
		return parser;
	}

	private class XlsxReader implements Runnable, RowHandler, Closeable {

		private XMLReader parser;
		private InputStream sheet;
		private int startAt = 0;
		int rowCount = 0;
		private Queue<Object[]> rows;
		private OPCPackage pkg;

		public XlsxReader(OPCPackage pkg, StylesTable styles, ReadOnlySharedStringsTable strings, InputStream sheet, Queue<Object[]> queue) {
			this.pkg = pkg;
			this.rows = queue;
			MyXSSFSheetHandler handler = new MyXSSFSheetHandler(styles, strings, 0);
			handler.setRowHandler(this);
			XMLReader parser;
			try {
				parser = fetchSheetParser(handler);
				this.parser = parser;
				this.sheet = sheet;
			} catch (SAXException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}

		public void close() throws IOException {
			_log.debug("Closing Xlsx reader");
			this.parser = null;
			IOUtils.closeQuietly(this.sheet);
			this.sheet = null;
			this.rows = null;

			pkg.revert();
			pkg.close();
			this.pkg = null;
		}

		public void setSkipRows(int skipRows) {
			this.startAt = skipRows;
		}

		@Override
		public void run() {
			try {
				InputSource sheetSource = new InputSource(sheet);
				_log.info("Xlsx reader thread starting to parse.");
				parser.parse(sheetSource);
				_log.info("Done parsing");
			} catch (StopParsingException e) {
				// NOP
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Throwable e) {
				_log.error(e.getMessage(), e);
			} finally {
				_log.debug("Background thread finished. Closing sheet.");
				try {
					sheet.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		public void handleRow(Object[] row) throws StopParsingException, InterruptedException {
			if (Thread.interrupted()) {
				throw new StopParsingException("Xlsx reader thread was interrupted. Stop parsing.");
			}

			rowCount++;

			if (startAt >= rowCount) {
				_log.debug("Skipping row " + rowCount + " startAt=" + startAt);
				return;
			}

			while (true) {
				try {
					rows.add(row);
					break;
				} catch (IllegalStateException e) {
					if (_log.isTraceEnabled())
						_log.trace("Pausing Xlsx parser, queue size=" + rows.size());
					Thread.sleep(10);
				}
			}
		}

	}
}
