package org.genesys2.anno.parser;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.genesys2.anno.model.DatabaseSettings;
import org.genesys2.anno.util.ConnectionUtils;

public class JdbcRowReader implements RowReader {

	private Connection conn = null;
	private String query;
	private PreparedStatement preparedStatement = null;
	private ResultSet res;
	private int columnCount;

	public JdbcRowReader(String query, DatabaseSettings databaseSettings) {
		this.query = query;
		String url = databaseSettings.getUrl();
		String driver = databaseSettings.getDriverClassName();
		String user = databaseSettings.getUser();
		String password = databaseSettings.getPassword();
		try {
			this.conn = ConnectionUtils.getConnection(driver, url, user, password);
		} catch (ClassNotFoundException e) {
		} catch (SQLException e) {
		}
		this.preparedStatement = null;
	}

	@Override
	public List<Object[]> readRows(int rowsToRead) throws IOException {
		try {
			if (this.preparedStatement == null) {
				this.preparedStatement = conn.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
				preparedStatement.setFetchSize(Integer.MIN_VALUE);
				this.res = preparedStatement.executeQuery();
				ResultSetMetaData metadata = res.getMetaData();
				this.columnCount = metadata.getColumnCount();
			}

			List<Object[]> rows = new ArrayList<Object[]>(rowsToRead);
			int rowCount = 0;
			while (res.next() && rowCount < rowsToRead) {
				List<Object> row = new ArrayList<Object>();
				for (int i = 1; i <= columnCount; i++) {
					Object val = res.getObject(i);
					row.add(val);
				}
				rows.add(row.toArray());
				rowCount++;
			}
			return rows;

		} catch (Throwable e) {
			try {
				this.preparedStatement.close();
			} catch (Throwable e1) {
				// NOOP
			}
			ConnectionUtils.close(this.conn);
			throw new IOException(e.getMessage(), e);
		}
	}

	@Override
	public void setSkipRows(int skipRows) {
		// ignored
	}

	@Override
	public void close() throws IOException {
		try {
			this.preparedStatement.close();
		} catch (SQLException e) {
			// NOOP
		}
		ConnectionUtils.close(this.conn);
	}

}
