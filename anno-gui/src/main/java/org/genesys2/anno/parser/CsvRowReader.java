package org.genesys2.anno.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.supercsv.io.CsvListReader;

public class CsvRowReader implements RowReader {
	private static final Logger _log = Logger.getLogger(CsvRowReader.class);

	private CsvListReader listReader;

	private int skipRows;

	public CsvRowReader(CsvListReader listReader) {
		this.listReader = listReader;
	}

	@Override
	public void setSkipRows(int skipRows) {
		this.skipRows = skipRows;
	}

	public int getSkipRows() {
		return skipRows;
	}

	@Override
	public void close() throws IOException {
		this.listReader.close();
	}

	@Override
	public synchronized List<Object[]> readRows(int rowsToRead) throws IOException {
		List<Object[]> rows = new ArrayList<Object[]>(rowsToRead);

		// Skip rows
		for (; skipRows > 0; skipRows--) {
			_log.debug("Skipping row ...");
			listReader.read();
		}
		for (int i = 0; i < rowsToRead; i++) {
			List<String> row = listReader.read();
			if (row == null)
				break;
			rows.add(row.toArray());
		}

		return rows;
	}

}
