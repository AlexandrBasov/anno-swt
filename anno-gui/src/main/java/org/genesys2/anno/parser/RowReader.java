package org.genesys2.anno.parser;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

public interface RowReader extends Closeable {

	List<Object[]> readRows(int rowsToRead) throws IOException;

	void setSkipRows(int skipRows);

}
