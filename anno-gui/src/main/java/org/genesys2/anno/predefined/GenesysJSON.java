/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.predefined;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

public class GenesysJSON {
	public static class JsonField {
		private String fieldName;
		private String rdfTerm;
		private boolean allowMultiple = false;
		private Class<?> type = String.class;
		private boolean required;

		public JsonField(String fieldName, String rdfTerm) {
			this.fieldName = fieldName;
			this.rdfTerm = rdfTerm;
		}

		public JsonField setAllowMultiple(boolean allowMultiple) {
			this.allowMultiple = allowMultiple;
			return this;
		}

		public boolean hasAllowMultiple() {
			return this.allowMultiple;
		}

		public String getFieldName() {
			return fieldName;
		}

		public String getRdfTerm() {
			return rdfTerm;
		}

		public Class<?> getType() {
			return this.type;
		}

		public JsonField setType(Class<?> type) {
			this.type = type;
			return this;
		}

		public JsonField setRequired(boolean b) {
			this.required = b;
			return this;
		}

		public boolean isRequired() {
			return required;
		}
	}

	private final List<JsonField> jsonFields;
	private Collection<JsonField> requiredFields;

	public GenesysJSON() {
		List<JsonField> columns = new ArrayList<JsonField>();
		JsonField instCode = new JsonField("instCode", RdfMCPD.INSTCODE).setRequired(true);
		columns.add(instCode);

		JsonField acceNumb = new JsonField("acceNumb", RdfMCPD.ACCENUMB).setRequired(true);
		columns.add(acceNumb);
		
		JsonField newAcceNumb = new JsonField("newAcceNumb", RdfMCPD.NEWACCENUMB).setRequired(false);
		columns.add(newAcceNumb);

		JsonField genus = new JsonField("genus", RdfMCPD.GENUS).setRequired(true);
		columns.add(genus);

		JsonField species = new JsonField("species", RdfMCPD.SPECIES);
		columns.add(species);

		JsonField columnDef;
		columnDef = new JsonField("spauthor", RdfMCPD.SPAUTHOR);
		columns.add(columnDef);

		columnDef = new JsonField("subtaxa", RdfMCPD.SUBTAXA);
		columns.add(columnDef);

		columnDef = new JsonField("subTauthor", RdfMCPD.SUBTAUTHOR);
		columns.add(columnDef);

		columnDef = new JsonField("acceName", RdfMCPD.ACCENAME).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new JsonField("acqDate", RdfMCPD.ACQDATE);
		columns.add(columnDef);

		columnDef = new JsonField("orgCty", RdfMCPD.ORIGCTY);
		columns.add(columnDef);

		columnDef = new JsonField("coll.collNumb", RdfMCPD.COLLNUMB);
		columns.add(columnDef);

		columnDef = new JsonField("coll.collDate", RdfMCPD.COLLDATE);
		columns.add(columnDef);

		columnDef = new JsonField("coll.collCode", RdfMCPD.COLLCODE).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new JsonField("coll.collName", RdfMCPD.COLLNAME).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new JsonField("coll.collSite", RdfMCPD.COLLSITE);
		columns.add(columnDef);

		columnDef = new JsonField("coll.collInstAddr", RdfMCPD.COLLINSTADDRESS).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new JsonField("coll.collMissId", RdfMCPD.COLLMISSID);
		columns.add(columnDef);

		columnDef = new JsonField("coll.collSrc", RdfMCPD.COLLSRC).setType(int.class);
		columns.add(columnDef);

		columnDef = new JsonField("geo.latitude", RdfMCPD.DECLATITUDE).setType(double.class);
		columns.add(columnDef);

		columnDef = new JsonField("geo.longitude", RdfMCPD.DECLONGITUDE).setType(double.class);
		columns.add(columnDef);

		columnDef = new JsonField("geo.coordUncert", RdfMCPD.COORDUNCERT).setType(double.class);
		columns.add(columnDef);

		columnDef = new JsonField("geo.coordDatum", RdfMCPD.COORDDATUM);
		columns.add(columnDef);

		columnDef = new JsonField("geo.georefMeth", RdfMCPD.GEOREFMETH);
		columns.add(columnDef);

		columnDef = new JsonField("geo.elevation", RdfMCPD.ELEVATION).setType(double.class);
		columns.add(columnDef);

		columnDef = new JsonField("bredCode", RdfMCPD.BREDCODE);
		columns.add(columnDef);

		columnDef = new JsonField("sampStat", RdfMCPD.SAMPSTAT).setType(int.class);
		columns.add(columnDef);

		columnDef = new JsonField("ancest", RdfMCPD.ANCEST);
		columns.add(columnDef);

		columnDef = new JsonField("donorCode", RdfMCPD.DONORCODE);
		columns.add(columnDef);

		columnDef = new JsonField("donorNumb", RdfMCPD.DONORNUMB);
		columns.add(columnDef);

		columnDef = new JsonField("donorName", RdfMCPD.DONORNAME);
		columns.add(columnDef);

		columnDef = new JsonField("otherNumb", RdfMCPD.OTHERNUMB).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new JsonField("duplSite", RdfMCPD.DUPLSITE).setAllowMultiple(true);
		columns.add(columnDef);

		// columnDef = new JsonField("duplInstName",
		// RdfMCPD.DUPLINSTNAME).setAllowMultiple(true);
		// columns.add(columnDef);

		columnDef = new JsonField("storage", RdfMCPD.STORAGE).setAllowMultiple(true).setType(int.class);
		columns.add(columnDef);

		columnDef = new JsonField("mlsStat", RdfMCPD.MLSSTAT).setType(boolean.class);
		columns.add(columnDef);

		columnDef = new JsonField("inTrust", RdfMCPD.FAOINTRUST).setType(boolean.class);
		columns.add(columnDef);

		columnDef = new JsonField("available", RdfMCPD.AVAILABLE).setType(boolean.class);
		columns.add(columnDef);

		columnDef = new JsonField("remarks", RdfMCPD.REMARKS).setAllowMultiple(true);
		columns.add(columnDef);

		this.jsonFields = Collections.unmodifiableList(columns);

		@SuppressWarnings("unchecked")
		Collection<JsonField> requiredFields = CollectionUtils.select(columns, new Predicate() {
			@Override
			public boolean evaluate(Object object) {
				JsonField jsonField = (JsonField) object;
				return jsonField.isRequired();
			}

		});
		this.requiredFields = Collections.unmodifiableCollection(requiredFields);
	}

	public final List<JsonField> getJsonFields() {
		return jsonFields;
	}

	public Collection<JsonField> getRequiredFields() {
		return requiredFields;
	}
}
