/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.predefined;

public class RdfMCPD {
	// http://purl.org/germplasm/germplasmTerm#
	public static final String INSTCODE = "http://rs.tdwg.org/dwc/terms/institutionCode";
	public static final String ACCENUMB = "http://purl.org/germplasm/germplasmTerm#germplasmID";
	
	public static final String NEWACCENUMB = "https://www.genesys-pgr.org/rdf/mcpd#newAcceNumb";

	public static final String GENUS = "http://rs.tdwg.org/dwc/terms/genus";
	public static final String SPECIES = "http://rs.tdwg.org/dwc/terms/specificEpithet";
	public static final String SPAUTHOR = "http://rs.tdwg.org/dwc/terms/infraspecificEpithetAuthorship";
	public static final String SUBTAXA = "http://rs.tdwg.org/dwc/terms/infraspecificEpithet";
	public static final String SUBTAUTHOR = "http://rs.tdwg.org/dwc/terms/scientificNameAuthorship";
	public static final String CROPNAME = "http://rs.tdwg.org/dwc/terms/vernacularName";
	public static final String ACCENAME = "http://purl.org/germplasm/germplasmTerm#germplasmIdentifier";
	public static final String ACQDATE = "http://purl.org/germplasm/germplasmTerm#acquisitionDate";
	public static final String ORIGCTY = "http://rs.tdwg.org/dwc/terms/country";
	public static final String COLLCODE = "http://purl.org/germplasm/germplasmTerm#collectingInstituteID";
	public static final String COLLNUMB = "http://rs.tdwg.org/dwc/terms/recordNumber";
	// made up
	public static final String COLLNAME = "http://purl.org/germplasm/germplasmTerm#collectingInstitute";
	public static final String COLLINSTADDRESS = "http://purl.org/germplasm/germplasmTerm#collectingInstituteAddress";
	public static final String COLLMISSID = "http://purl.org/germplasm/germplasmTerm#collectingMissionID";
	public static final String COLLDATE = "http://purl.org/germplasm/germplasmTerm#collectingDate";
	public static final String GEOREFMETH = "http://purl.org/germplasm/germplasmTerm#georeferencingMethod";
	// -- end made up
	public static final String COLLSITE = "http://rs.tdwg.org/dwc/terms/verbatimLocality";
	public static final String DECLATITUDE = "http://rs.tdwg.org/dwc/terms/decimalLatitude";
	public static final String DECLONGITUDE = "http://rs.tdwg.org/dwc/terms/decimalLongitude";
	public static final String COORDUNCERT = "http://rs.tdwg.org/dwc/terms/coordinateUncertaintyInMeters";
	public static final String COORDDATUM = "http://rs.tdwg.org/dwc/terms/geodeticDatum";
	public static final String ELEVATION = "http://www.w3.org/2003/01/geo/wgs84_pos#alt";
	public static final String BREDCODE = "http://purl.org/germplasm/germplasmTerm#breedingInstituteID";
	public static final String SAMPSTAT = "http://purl.org/germplasm/germplasmTerm#biologicalStatus";
	public static final String ANCEST = "http://purl.org/germplasm/germplasmTerm#ancestralData";
	public static final String COLLSRC = "http://purl.org/germplasm/germplasmTerm#acquisitionSource";
	public static final String DONORCODE = "http://purl.org/germplasm/germplasmTerm#donorInstituteID";
	public static final String DONORNAME = "http://purl.org/germplasm/germplasmTerm#donorInstitute";
	public static final String DONORNUMB = "http://purl.org/germplasm/germplasmTerm#donorsIdentifier";
	public static final String OTHERNUMB = "http://rs.tdwg.org/dwc/terms/otherCatalogNumbers";
	public static final String DUPLSITE = "http://purl.org/germplasm/germplasmTerm#safetyDuplicationInstituteID";
	public static final String DUPLINSTNAME = "http://purl.org/germplasm/germplasmTerm#safetyDuplicationInstitute";
	public static final String STORAGE = "http://purl.org/germplasm/germplasmType#storageCondition";
	public static final String MLSSTAT = "http://purl.org/germplasm/germplasmTerm#mlsStatus";
	public static final String REMARKS = "http://rs.tdwg.org/dwc/terms/occurrenceRemarks";

	public static final String FAOINTRUST = "https://www.genesys-pgr.org/rdf/passport/faoInTrust";
	public static final String AVAILABLE = "https://www.genesys-pgr.org/rdf/passport/available";

}
