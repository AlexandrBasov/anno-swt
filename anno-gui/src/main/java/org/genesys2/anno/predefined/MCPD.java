/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.anno.predefined;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.genesys2.anno.model.ColumnDataType;
import org.genesys2.anno.model.ColumnDef;
import org.genesys2.anno.validator.FaoWiewsInstCodeValidator;

public class MCPD {

	private final List<ColumnDef> columnDefs;

	public MCPD() {
		List<ColumnDef> columns = new ArrayList<ColumnDef>();
		ColumnDef instCode = new ColumnDef("INSTCODE", "Institute code", ColumnDataType.TEXT, false, RdfMCPD.INSTCODE);
		instCode.getValidators().add(new FaoWiewsInstCodeValidator());
		columns.add(instCode);

		ColumnDef acceNumb = new ColumnDef("ACCENUMB", "Accession number", ColumnDataType.TEXT, true, RdfMCPD.ACCENUMB);
		columns.add(acceNumb);

		ColumnDef newAcceNumb = new ColumnDef("NEWACCENUMB", "New accession number", ColumnDataType.TEXT, true, RdfMCPD.NEWACCENUMB);
		columns.add(newAcceNumb);

		ColumnDef columnDef = new ColumnDef("COLLNUMB", "Collecting number", ColumnDataType.TEXT, false, RdfMCPD.COLLNUMB);
		columns.add(columnDef);

		columnDef = new ColumnDef("COLLCODE", "Collecting institute code", ColumnDataType.TEXT, false, RdfMCPD.COLLCODE).setAllowMultiple(true);
		columnDef.getValidators().add(new FaoWiewsInstCodeValidator());
		columns.add(columnDef);

		columnDef = new ColumnDef("COLLNAME", "Collecting institute name", ColumnDataType.TEXT, false, RdfMCPD.COLLNAME).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new ColumnDef("COLLINSTADDRESS", "Collecting institute address", ColumnDataType.TEXT, false, RdfMCPD.COLLINSTADDRESS).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new ColumnDef("COLLMISSID", "Collecting mission identifier", ColumnDataType.TEXT, false, RdfMCPD.COLLMISSID);
		columns.add(columnDef);

		ColumnDef genus = new ColumnDef("GENUS", "Genus", ColumnDataType.TEXT, false, RdfMCPD.GENUS);
		columns.add(genus);

		ColumnDef species = new ColumnDef("SPECIES", "Species", ColumnDataType.TEXT, false, RdfMCPD.SPECIES);
		columns.add(species);

		columnDef = new ColumnDef("SPAUTHOR", "Species authority", ColumnDataType.TEXT, false, RdfMCPD.SPAUTHOR);
		columns.add(columnDef);

		columnDef = new ColumnDef("SUBTAXA", "Subtaxon", ColumnDataType.TEXT, false, RdfMCPD.SUBTAXA);
		columns.add(columnDef);

		columnDef = new ColumnDef("SUBTAUTHOR", "Subtaxon authority", ColumnDataType.TEXT, false, RdfMCPD.SUBTAUTHOR);
		columns.add(columnDef);

		columnDef = new ColumnDef("CROPNAME", "Common crop name", ColumnDataType.TEXT, false, RdfMCPD.CROPNAME);
		columns.add(columnDef);

		columnDef = new ColumnDef("ACCENAME", "Accession name", ColumnDataType.TEXT, false, RdfMCPD.ACCENAME).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new ColumnDef("ACQDATE", "Acquisition date", ColumnDataType.TEXT, false, RdfMCPD.ACQDATE);
		columns.add(columnDef);

		columnDef = new ColumnDef("ORIGCTY", "Country of origin", ColumnDataType.TEXT, false, RdfMCPD.ORIGCTY);
		columns.add(columnDef);

		columnDef = new ColumnDef("COLLSITE", "Location of collecting site", ColumnDataType.TEXT, false, RdfMCPD.COLLSITE);
		columns.add(columnDef);

		columnDef = new ColumnDef("DECLATITUDE", "Latitude of collecting site (decimal)", ColumnDataType.DOUBLE, false, RdfMCPD.DECLATITUDE);
		columns.add(columnDef);

		columnDef = new ColumnDef("DECLONGITUDE", "Longitude of collecting site (decimal)", ColumnDataType.DOUBLE, false, RdfMCPD.DECLONGITUDE);
		columns.add(columnDef);

		columnDef = new ColumnDef("LATITUDED", "Latitude of collecting site (decimal)", ColumnDataType.DOUBLE, false, RdfMCPD.DECLATITUDE);
		columns.add(columnDef);

		columnDef = new ColumnDef("LONGITUDED", "Longitude of collecting site (decimal)", ColumnDataType.DOUBLE, false, RdfMCPD.DECLONGITUDE);
		columns.add(columnDef);

		columnDef = new ColumnDef("COORDUNCERT", "Coordinate uncertainty", ColumnDataType.DOUBLE, false, RdfMCPD.COORDUNCERT);
		columns.add(columnDef);

		columnDef = new ColumnDef("COORDDATUM", "Coordinate datum", ColumnDataType.TEXT, false, RdfMCPD.COORDDATUM);
		columns.add(columnDef);

		columnDef = new ColumnDef("GEOREFMETH", "Georeferencing method", ColumnDataType.TEXT, false, RdfMCPD.GEOREFMETH);
		columns.add(columnDef);

		columnDef = new ColumnDef("ELEVATION", "Elevation of collecting site", ColumnDataType.DOUBLE, false, RdfMCPD.ELEVATION);
		columns.add(columnDef);

		columnDef = new ColumnDef("COLLDATE", "Collecting date of sample", ColumnDataType.TEXT, false, RdfMCPD.COLLDATE);
		columns.add(columnDef);

		columnDef = new ColumnDef("BREDCODE", "Breeding institute code", ColumnDataType.TEXT, false, RdfMCPD.BREDCODE);
		columnDef.getValidators().add(new FaoWiewsInstCodeValidator());
		columns.add(columnDef);

		columnDef = new ColumnDef("SAMPSTAT", "Biological status of accession", ColumnDataType.TEXT, false, RdfMCPD.SAMPSTAT);
		columns.add(columnDef);

		columnDef = new ColumnDef("ANCEST", "Ancestral data", ColumnDataType.TEXT, false, RdfMCPD.ANCEST);
		columns.add(columnDef);

		columnDef = new ColumnDef("COLLSRC", "Collecting/acquisition source", ColumnDataType.TEXT, false, RdfMCPD.COLLSRC);
		columns.add(columnDef);

		columnDef = new ColumnDef("DONORCODE", "Donor institute code", ColumnDataType.TEXT, false, RdfMCPD.DONORCODE);
		columnDef.getValidators().add(new FaoWiewsInstCodeValidator());
		columns.add(columnDef);

		columnDef = new ColumnDef("DONORNAME", "Donor institute name", ColumnDataType.TEXT, false, RdfMCPD.DONORNAME);
		columns.add(columnDef);

		columnDef = new ColumnDef("DONORNUMB", "Donor accession number", ColumnDataType.TEXT, false, RdfMCPD.DONORNUMB);
		columns.add(columnDef);

		columnDef = new ColumnDef("OTHERNUMB", "Other identifiers associated with the accession", ColumnDataType.TEXT, false, RdfMCPD.OTHERNUMB).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new ColumnDef("DUPLSITE", "Location of safety duplicates", ColumnDataType.TEXT, false, RdfMCPD.DUPLSITE).setAllowMultiple(true);
		columnDef.getValidators().add(new FaoWiewsInstCodeValidator());
		columns.add(columnDef);

		columnDef = new ColumnDef("DUPLINSTNAME", "Institute maintaining safety duplicates", ColumnDataType.TEXT, false, RdfMCPD.DUPLINSTNAME).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new ColumnDef("STORAGE", "Type of germplasm storage", ColumnDataType.TEXT, false, RdfMCPD.STORAGE).setAllowMultiple(true);
		columns.add(columnDef);

		columnDef = new ColumnDef("MLSSTAT", "MLS status of the accession", ColumnDataType.BOOLEAN, false, RdfMCPD.MLSSTAT);
		columns.add(columnDef);

		columnDef = new ColumnDef("REMARKS", "Remarks", ColumnDataType.TEXT, false, RdfMCPD.REMARKS).setAllowMultiple(true);
		columns.add(columnDef);

		// Genesys specific
		columnDef = new ColumnDef("INTRUST", "FAO in Trust", ColumnDataType.BOOLEAN, false, RdfMCPD.FAOINTRUST);
		columns.add(columnDef);

		columnDef = new ColumnDef("AVAILABLE", "Accession is available for distribution", ColumnDataType.BOOLEAN, false, RdfMCPD.AVAILABLE);
		columns.add(columnDef);

		this.columnDefs = Collections.unmodifiableList(columns);
	}

	public final List<ColumnDef> getColumnDefs() {
		return columnDefs;
	}
}
